video_server
============

The idea is to build a multi-threaded server that streams sequences of video frames, via sockets,  to client threads. You should start by implementing a server that deals with text-based requests from clients and then add the support for video later. An advanced part of the project involves the implementation of a monitor abstraction that supports an arbitrary list of functions, initialization code, shared data structures and a series of queues for condition variables and entry into the monitor. 

===============

Contributions:

Michelle Beard:
- Thread pool management
    - Create new threads as client requests are accepted.
    - Delete idle threads after allotted time T. 
- Message Demo (Client & Server) example   
    - Demonstrates the producer/consumer model with Worker threads 
      generating messages and adding them to a shared a circular buffer where
      the Dispatcher removes those messages and sends them off to the 
      appropriate client.
    - The shared circular buffer is sorted by client priority before the 
      Dispatcher sends off the message.
- Monitor Synchronization
    - Implemented monitor abstraction layer. Shared objects between the 
      Dispatcher and Worker threads (buffer, conditional variables, mutexes). 
      

Calvin Flegal:
- Message Class
    - Custom class serializer for sending data messages over a socket.
- Video Support (not included in submission)
    - Custom class methods for serializing buffers with videos
    - Though this serialization passed testing, never integrated with
      local filesystem and is thus omitted.

John Ying:
- Queue class
    - Queue data structure with atomic operations (e.g., add, remove, at)
- Mutex kernel module
    - Kernel module that contains mutexes and conditional variables to be used
      by user-space applications
    - Accepts IOCTL calls as system calls for the following operations:
      lock/unlock mutex, and conditional wait/signal
- Demo that generates 100 messages using system calls for mutexes
    - Same as Part 1 but uses system calls instead of pthread mutexes
