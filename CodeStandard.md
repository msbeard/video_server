1. PLEASE do not commit binary files! Keep everything organized and well documented.

2. We will be coding in C++, and lets stick to an OOP programming style. 
   All interfaces are defined in the header file and the implementation in the cpp file. 
   Put #ifndef guards in the header files to avoid double inclusion. 

3. Document everything! 

4. Class library names are in the form of NameOfClass.

5. Private variables should start with m_variableName. 

6. Use the typedefs defined in Types.hpp under the Util directory. 

7. Before committing any changes and pushing them to the repo, make sure your source code can build. 

*. We can extend this once we agree on a format.

-- Michelle
