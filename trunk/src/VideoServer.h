// Written by Michelle Beard

#ifndef VS_HEADER_HPP
#define VS_HEADER_HPP

// All includes
#include "CondVar/CondVar.hpp"
#include "Message/Message.hpp"
#include "Mutex/Mutex.hpp"
#include "Queue/Queue.hpp"
#include "Thread/Thread.hpp"
#include "ThreadPool/ThreadPool.hpp"
#include "Util/Types.hpp"

/*
 * TCP Class Borrowed from
 * http://vichargrave.com/network-programming-design-patterns-in-c/
 * License: Apache
 */

#include "TCPSockets/tcpacceptor.h"
#include "TCPSockets/tcpconnector.h"
#include "TCPSockets/tcpstream.h"


#endif /*VS_HEADER_HPP */
