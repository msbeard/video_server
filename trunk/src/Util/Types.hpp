#ifndef TYPES_HEADER_H
#define TYPES_HEADER_H

#define FALSE 0
#define TRUE 1
#define DEBUG FALSE

typedef unsigned char uint8;
typedef unsigned short int uint16;
// typedef unsigned long uint;
typedef unsigned long int uint32;
typedef unsigned long long int uint64;

typedef signed char sint8, s8;
typedef signed short int sint16, s16;
typedef signed long sint;
typedef signed long int sint32, s32;
typedef signed long long int sint64, s64;

typedef const char * string;

#ifndef _SIZE_T
typedef int size_t;
#define _SIZE_T 1
#endif

// Mutex ID data structure
struct my_mutex_id {
    uint32 mutex;
    uint32 condv;
};

// IOCTL codes for mutex calls
#define MY_MUTEX_LOCK   _IOW(0, 0, my_mutex_id)
#define MY_MUTEX_UNLOCK _IOW(0, 1, my_mutex_id)
#define MY_COND_WAIT    _IOW(0, 2, my_mutex_id)
#define MY_COND_SIGNAL  _IOW(0, 3, my_mutex_id)

#endif /* TYPES_HEADER_H */
