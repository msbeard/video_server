#ifndef MESSAGE_HEADER_HPP
#define MESSAGE_HEADER_HPP

#include <vector>
#include <string>
#include "../Util/Types.hpp"

/* Clients send messages to the server in the form: 
 * "client ID:priority:request:arguments"
 */
class Message 
{
private:
    uint32 m_clientID;
    uint32 m_priority;
    std::string m_request;
    uint32 m_argumentsCount;
    std::vector<std::string> m_arguments;
    char* m_imageFileBuffer;
    uint32 m_imageFileBufferSize;
    
public:
    //method to ship
    const char * serialize();

    const char * serializeVideoMessage();


    Message ();
    ~Message();
    
    Message(uint32 clientID,
            uint32 priority, 
            std::string request,
            std::vector<std::string> arguments, uint32 argumentsCount);
    
    //Video constructor
    Message(uint32 clientID,
                  uint32 priority,
                  std::string request,
                  uint32 vidSize,
                  char* videoBuffer,
                  std::vector<std::string> arguments,
                  uint32 argumentsCount);
    Message deSerialize(const char* serializedMessage);
    void deSerialize2 (const char* serialzedMessage);
    void deSerializeVideoMessage (const char* serializedMessage);

    
    // comparer 
    bool operator() (const Message &a, const Message &b) const;
    
    //getters 
    char const *getRequest();
    std::vector<std::string> getArguments();
    uint32 getClientID();
    uint32 getPriority();
    uint32 getArgumentsCount();
};

#endif /*MESSAGE_HEADER_HPP */
