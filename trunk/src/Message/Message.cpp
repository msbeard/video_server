#include "Message.hpp"
#include <sstream>
#include <iostream>
#include <cstring>
#include <cstdio>
Message::Message ()
{
    m_clientID = 0;
    m_priority = 0;
    m_request = "";
//    m_arguments = arguments;
    m_argumentsCount = 0;
    m_imageFileBufferSize = 0;
}

Message::Message (uint32 clientID,
                  uint32 priority,
                  std::string request,
                  std::vector<std::string> arguments,
                  uint32 argumentsCount)
{
    m_clientID = clientID;
    m_priority = priority;
    m_request = request;
    m_arguments = arguments;
    m_argumentsCount = argumentsCount;
    m_imageFileBufferSize = 0;
}
//Video constructor
Message::Message(uint32 clientID,
                  uint32 priority,
                  std::string request,
                  uint32 vidSize,
                  char* videoBuffer,
                  std::vector<std::string> arguments,
                  uint32 argumentsCount) {
    m_clientID = clientID;
    m_priority = priority;
    m_request = request;
    m_arguments = arguments;
    m_argumentsCount = argumentsCount;
    m_imageFileBuffer = videoBuffer;
    m_imageFileBufferSize = vidSize;
}
 Message::~Message ()
 {
     if (m_imageFileBufferSize != 0)
        delete[] m_imageFileBuffer;
 }

bool Message::operator() (const Message &a, const Message& b) const
{
    if (a.m_priority == b.m_priority)
        return a.m_clientID < b.m_clientID;
    return a.m_priority < b.m_priority;
}

const char* Message::serializeVideoMessage() {
        std::vector<unsigned char> output;
    //add 8 byte header placeholder
    //it will tell us size of messsage INCLUDING header
    //
    uint32 sizeOf32 = sizeof(uint32);
    for (int j=0; j<sizeOf32; j++)
     output.push_back(0x00);

    output.push_back (sizeof (uint32)); //sizeof uint32 //offset 0
    for (int i = 0; i < sizeof (uint32) - 1; i++)
        output.push_back (0x00); //we'll want the other 7 bytes to be zero
    //push the actual value starts at output[8]

    uint32 copyOfID = m_clientID;
    for (int j = 0; j < sizeof (uint32); j++)
    {
        char this_byte = 0xFF & copyOfID;
        output.push_back (this_byte);
        copyOfID = copyOfID >> 8;
    }
    //push the priority size/variable
    output.push_back (sizeof (uint32));
    for (int i = 0; i < sizeof (uint32) - 1; i++)
        output.push_back (0x00); //we'll want the other 7 bytes to be zero
    //push the var at offset output[16]
    uint32 copyOfPriority = m_priority;
    for (int j = 0; j<sizeof (uint32); j++)
    {
        char this_byte = 0xFF & copyOfPriority;
        output.push_back (this_byte);
        copyOfPriority = copyOfPriority >> 8;
    }
    //at output[32] we expect the first space delimiter
    output.push_back (' ');
    // push the request...we wont actually push sizes
    for (int j = 0; j < m_request.length (); j++)
    {
        output.push_back (m_request[j]);
    }
    output.push_back (' ');

    //HERE we will write in the sizeof(buffersize).
    //
     output.push_back (sizeof (uint32));
    for (int i = 0; i<sizeof (uint32) - 1; i++)
        output.push_back (0x00); //we'll want the other 7 bytes to be zero

    //next the actual buffer size
    uint32 copyBuffSize = m_imageFileBufferSize;
    for (int j = 0; j<sizeof (uint32); j++)
    {
        char this_byte = 0xFF & copyBuffSize;
        output.push_back (this_byte);
        copyBuffSize = copyBuffSize >> 8;
    }
    //copy the image buffer
    for (int i=0; i< m_imageFileBufferSize; i++) {
        output.push_back(m_imageFileBuffer[i]);
    }


    //push back the arg count size
    output.push_back (sizeof (uint32));
    for (int i = 0; i<sizeof (uint32) - 1; i++)
        output.push_back (0x00); //we'll want the other 7 bytes to be zero

    //push back the arg count
    //// so, the start of this number will be at 32+request.size+2(delimiters)+8(sizeof(unit32))
    uint32 numberargsCopy = m_argumentsCount;
    for (int j = 0; j<sizeof (uint32); j++)
    {
        char this_byte = 0xFF & numberargsCopy;
        output.push_back (this_byte);
        numberargsCopy = numberargsCopy >> 8;
    }
    //for now we will hardcode 3 arguments...if we need more uh whatever. it will screw
    //up scanf to have variable (format string)...
    for (int j = 0; j < 3; j++)
    {
        output.push_back (' ');
        std::string this_argument = m_arguments[j];
        for (int k = 0; k < this_argument.length (); k++)
        {
            output.push_back (this_argument[k]);

        }
        output.push_back (' ');
    }
    output.push_back ('\0');
    //fill in header
    uint32 size = output.size();
    uint32 copyOfsize = size;
    for (int j=0; j<sizeOf32; j++) {
        char this_byte = 0xFF & copyOfsize;
        output[j] = this_byte;
        copyOfsize = copyOfsize >> 8;
    }
    char* serial = (char*) &output[0];
    //this is the clientId
    return serial;

}

void Message::deSerializeVideoMessage (const char* serializedMessage) {

    //skip header
        //we'll cheat and skip some sizes for uint32s.
    uint32 sizeOf32 = sizeof (uint32);
    char clientID[sizeOf32];
    memcpy (clientID, &serializedMessage[(2*sizeOf32)], sizeOf32);
    uint32 uclientID = *((uint32*) clientID);

    char priority[sizeOf32];
    memcpy (priority, &serializedMessage[(4*sizeOf32)], sizeOf32);
    uint32 upriority = *((uint32*) priority);


    char* startofRequest = (char*) &serializedMessage[(5*sizeOf32)];
    char requestbuffer[64];
    sscanf (startofRequest, "%s", requestbuffer);
    std::string request (requestbuffer);

    //compute the number of bytes between request and start of video size
    int bytesToBuffSize = (strlen (requestbuffer) + 2+sizeOf32); //delimters and 1x sizeof uint32
    char*startofvidSize = (char*) &startofRequest[bytesToBuffSize];

    char vidSize[sizeof(uint32)];
    memcpy(vidSize,startofvidSize,sizeof(uint32));
    uint32 videoSize = *((uint32*)vidSize);

    char* startofVideo = (char*) &startofvidSize[sizeof(uint32)];
    //now we need to allocate space for the video.
    m_imageFileBuffer = new char[videoSize];
    //now copy the video buffer there...
    memcpy(m_imageFileBuffer,startofVideo,videoSize);

    char* startofargs = (char*)&startofVideo[((videoSize) +2*sizeOf32)]; //two uints makes 16 (argconut and sizeofargcount)

    char arg1[32];
    char arg2[32];
    char arg3[32];
    sscanf (startofargs, " %s %s %s ", arg1, arg2, arg3);
    std::vector<std::string>args;
    args.push_back (std::string (arg1));
    args.push_back (std::string (arg2));
    args.push_back (std::string (arg3));
    
    m_clientID = uclientID;
    m_priority = upriority;
    m_request = request;
    m_arguments = args;
    m_argumentsCount = args.size();
    m_imageFileBufferSize = videoSize;


}
const char* Message::serialize ()
{
    std::vector<unsigned char> output;
    output.push_back (sizeof (uint32)); //sizeof uint32 //offset 0
    for (int i = 0; i < sizeof (uint32) - 1; i++)
        output.push_back (0x00); //we'll want the other 7 bytes to be zero
    //push the actual value starts at output[8]

    uint32 copyOfID = m_clientID;
    for (int j = 0; j < sizeof (uint32); j++)
    {
        char this_byte = 0xFF & copyOfID;
        output.push_back (this_byte);
        copyOfID = copyOfID >> 8;
    }
    //push the priority size/variable
    output.push_back (sizeof (uint32));
    for (int i = 0; i < sizeof (uint32) - 1; i++)
        output.push_back (0x00); //we'll want the other 7 bytes to be zero
    //push the var at offset output[16]
    uint32 copyOfPriority = m_priority;
    for (int j = 0; j<sizeof (uint32); j++)
    {
        char this_byte = 0xFF & copyOfPriority;
        output.push_back (this_byte);
        copyOfPriority = copyOfPriority >> 8;
    }
    //at output[32] we expect the first space delimiter
    output.push_back (' ');
    // push the request...we wont actually push sizes
    for (int j = 0; j < m_request.length (); j++)
    {
        output.push_back (m_request[j]);
    }
    output.push_back (' ');
    //push back the arg count size
    output.push_back (sizeof (uint32));
    for (int i = 0; i<sizeof (uint32) - 1; i++)
        output.push_back (0x00); //we'll want the other 7 bytes to be zero

    //push back the arg count
    //// so, the start of this number will be at 32+request.size+2(delimiters)+8(sizeof(unit32))
    uint32 numberargsCopy = m_argumentsCount;
    for (int j = 0; j<sizeof (uint32); j++)
    {
        char this_byte = 0xFF & numberargsCopy;
        output.push_back (this_byte);
        numberargsCopy = numberargsCopy >> 8;
    }
    //for now we will hardcode 3 arguments...if we need more uh whatever. it will screw
    //up scanf to have variable (format string)...
    for (int j = 0; j < 3; j++)
    {
        output.push_back (' ');
        std::string this_argument = m_arguments[j];
        for (int k = 0; k < this_argument.length (); k++)
        {
            output.push_back (this_argument[k]);

        }
        output.push_back (' ');
    }
    output.push_back ('\0');

    char* serial = (char*) &output[0];
    //this is the clientId
    return serial;
}

Message Message::deSerialize (const char* serializedMessage)
{
    //we'll cheat and skip some sizes for uint32s.
    uint32 sizeOf32 = sizeof (uint32);
    char clientID[sizeOf32];
    memcpy (clientID, &serializedMessage[sizeOf32], sizeOf32);
    uint32 uclientID = *((uint32*) clientID);

    char priority[sizeOf32];
    memcpy (priority, &serializedMessage[24], sizeOf32);
    uint32 upriority = *((uint32*) priority);


    char* startofRequest = (char*) &serializedMessage[32];
    char requestbuffer[64];
    sscanf (startofRequest, "%s", requestbuffer);
    std::string request (requestbuffer);

    int bytesToargs = strlen (requestbuffer) + 18; //delimters and 2x sizeof uint32
    char*startofargs = (char*) &startofRequest[bytesToargs];

    char arg1[32];
    char arg2[32];
    char arg3[32];
    sscanf (startofargs, " %s %s %s ", arg1, arg2, arg3);
    std::vector<std::string>args;
    args.push_back (std::string (arg1));
    args.push_back (std::string (arg2));
    args.push_back (std::string (arg3));

    Message m1 (uclientID, upriority, request, args, 3);
    return m1;
}

void Message::deSerialize2 (const char* serializedMessage)
{
    //we'll cheat and skip some sizes for uint32s.
    uint32 sizeOf32 = sizeof (uint32);
    char clientID[sizeOf32];
    memcpy (clientID, &serializedMessage[sizeOf32], sizeOf32);
    uint32 uclientID = *((uint32*) clientID);

    char priority[sizeOf32];
    memcpy (priority, &serializedMessage[(3*sizeOf32)], sizeOf32);
    uint32 upriority = *((uint32*) priority);


    char* startofRequest = (char*) &serializedMessage[(4*sizeOf32)];
    char requestbuffer[64];
    sscanf (startofRequest, "%s", requestbuffer);
    std::string request (requestbuffer);

    int bytesToargs = (strlen (requestbuffer) + 2+ 2*sizeOf32); //delimters and 2x sizeof uint32
    char*startofargs = (char*) &startofRequest[bytesToargs];

    char arg1[32];
    char arg2[32];
    char arg3[32];
    sscanf (startofargs, " %s %s %s ", arg1, arg2, arg3);
    std::vector<std::string>args;
    args.push_back (std::string (arg1));
    args.push_back (std::string (arg2));
    args.push_back (std::string (arg3));
    
    m_clientID = uclientID;
    m_priority = upriority;
    m_request = request;
    m_arguments = args;
    m_argumentsCount = args.size();
}

char const * Message::getRequest ()
{
    return m_request.c_str ();
}

std::vector<std::string> Message::getArguments ()
{
    return m_arguments;
}

uint32 Message::getClientID ()
{
    return m_clientID;
}

uint32 Message::getPriority ()
{
    return m_priority;
}

uint32 Message::getArgumentsCount ()
{
    return m_argumentsCount;

}

