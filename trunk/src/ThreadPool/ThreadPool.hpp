/* 
 * File:   ThreadPool.hpp
 * Author: msbeard
 *
 * References: 
 * http://cppthreadpool.googlecode.com/
 * http://www.cs.wustl.edu/~schmidt/PDF/C++-report-col6.pdf
 * https://github.com/maginatics/threadpool
 * Created on November 5, 2013, 2:51 PM
 */

#ifndef THREADPOOL_HEADER_HPP
#define	THREADPOOL_HEADER_HPP

#include <stdio.h>
#include <vector>
#include <semaphore.h>
#include <map>
#include "../VideoServer.h"

/*
 * StubThread class
 * This class needs to be sub-classed by the user.
 */
class StubThread
{
public:
    
    unsigned virtual executeThis(uint32 id)
    {
        printf ("In StubThread virtual execute this\n");
        return 0;
    }
    
    StubThread ()
    {
        
    }

    virtual ~StubThread()
    {
    }
};

class ThreadPool
{
private:
    /* Initial number of threads in pool */
    uint32 m_threads;
    
    /* Maximum number of threads in pool */
    uint32 m_maxThreads;

    /* Number of seconds to keep a thread alive in pool */
    uint32 m_keepAlive;

    /* Array of Stubs */
    Queue<StubThread*> m_pool;
    
    /* Map of Worker Threads */    
    std::map <uint32, Thread*> m_threadMap;
    
    /* Pool Size */
    uint32 m_poolSize;

    /* Pool Indices */
    uint32 m_topIdx;
    uint32 m_bottomIdx;

    /* Mutex/CondVar for syncing the pool */
    Mutex *m_mutexSyncPool;
    Mutex *m_mutexWorkIncomplete;
    CondVar *m_condAvailWork;
    CondVar *m_condAvailThreads;

    /* Keep track of incomplete work */
    uint32 m_incomplete;

    /* Disallow copying */
    ThreadPool(const ThreadPool& orig);

public:
    /* Thread Pool constructor with maxThreads */
    ThreadPool(uint32 minThreads, uint32 maxThreads, uint32 keepAlive);

    /* Destroy all threads in pool */
    virtual ~ThreadPool();

    /* Initialize pool with m_threads */
    void init();

    /* Destroy surplus idle threads after some allotted time */    
    void destroyThread ();

    /* Assign a thread work in the pool */
    bool assignWork(StubThread* th);

    /* Fetch the next available thread */
    bool fetchWork(StubThread** th);

    /* Execute Thread Pool */
    void * run(uint32 id);

    /* Add an idle thread to the pool */
    bool addThread();
    
    /* Return the size of the work queue */
    uint32 getPoolSize ();
    
    /* Return the time to keep a thread alive */
    uint32 getTimeout ();
};

#endif	/* THREADPOOL_HEADER_HPP */