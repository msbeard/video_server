/*
 * File:   ThreadPool.cpp
 * Author: msbeard
 *
 * Created on November 5, 2013
 */

#include "ThreadPool.hpp"
#include <errno.h>

#define MINNUMTHREADS 2
#define MAXNUMTHREADS 25
#define DESTROY 1

/*
 * WorkerThread:
 * 
 * Feature: Each thread attempts to grab work from the shared buffer
 * m_pool, execute work, and try to grab more. Otherwise, block
 * until more work is added by ThreadPool manager. 
 * 
 */
class WorkerThread : public Thread
{
private:
    ThreadPool *m_tp;

public:

    WorkerThread (ThreadPool *tp) : m_tp (tp) { };

    void * run ()
    {
        printf ("ThreadPool::run from Worker start %lu\n", (uint32) this->self ());
        m_tp->run ((uint32) this->self ());
    }
};

/*
 * ThreadPool: 
 * 
 * Feature: Dynamically create more threads if the number of threads is
 * less than the number of requests. 
 * 
 * Feature: After some allotted time T, an idle thread will shut down. 
 * 
 */
ThreadPool::ThreadPool (uint32 minThreads, uint32 maxThreads, uint32 keepAlive)
{
    /* Initial number of Min Threads */
    if (minThreads > MINNUMTHREADS)
        m_threads = MINNUMTHREADS;
    else m_threads = minThreads;

    /* Initial Number of Max Threads */
    if (maxThreads > MAXNUMTHREADS)
        m_maxThreads = MAXNUMTHREADS;
    else m_maxThreads = maxThreads;

    /* Number of seconds to keep a thread alive in a pool */
    m_keepAlive = keepAlive;

    /* Incomplete Work */
    m_incomplete = 0;

    /* Initialize the Mutexes */
    m_mutexSyncPool = new Mutex ();
    m_mutexWorkIncomplete = new Mutex ();

    /* Initialize the Conditional Variables */
    m_condAvailThreads = new CondVar (*m_mutexSyncPool);
    m_condAvailWork = new CondVar (*m_mutexSyncPool);
}

void ThreadPool::init ()
{
    m_mutexSyncPool->lock ();

    for (uint32 i = 0; i < m_threads; i++)
        addThread ();

    if (DEBUG)
        printf ("Constructed ThreadPool of size %lu\n", m_threadMap.size ());

    /* Iterate over threads to print their ids */
    if (DEBUG)
    {
        std::map<uint32, Thread*>::iterator it;
        for (it = m_threadMap.begin (); it != m_threadMap.end (); ++it)
        {
            printf ("Thread[%lu] in map\n", it->first);
        }
    }

    m_mutexSyncPool->unlock ();
}

bool ThreadPool::assignWork (StubThread* th)
{
    /* 1. Every time work is assigned, increase incomplete work.
     * 2. Verify if buffer is not full. Otherwise, wait until N
     *    available slots are open.
     * 3. Find available thread to do work.
     */

    m_mutexWorkIncomplete->lock ();
    m_incomplete++;
    if (DEBUG)
        printf ("Work incomplete = %d\n", m_incomplete);
    m_mutexWorkIncomplete->unlock ();

    m_mutexSyncPool->lock ();
    printf ("Acquire lock in ThreadPool::assignWork ()\n");

    while (m_pool.size () == m_threads) /* @todo This seems a bit odd. */
    {
        if (DEBUG)
            printf ("Blocking until thread pool slots are open");
        m_condAvailThreads->wait ();
    }

    m_pool.add (th); // push back to end 

    printf ("Release lock in ThreadPool::assignWork ()\n");
    m_mutexSyncPool->unlock ();

    m_condAvailWork->signal ();
    return TRUE;
}

bool ThreadPool::fetchWork (StubThread** th)
{
    m_mutexSyncPool->lock ();
    if (DEBUG)
        printf ("Acquire lock in ThreadPool::fetchWork ()\n");

    while (m_pool.empty ())
    {
        printf ("Waiting for more work to come! Pool = %d\n", m_pool.size ());
        /*
         * While a thread waits for work to come,
         * possibly try to destroy it if it remains idle for more 
         * than time T
         */
        uint32 res;
        res = m_condAvailWork->timedwait (m_keepAlive);
        if (res == ETIMEDOUT)
        {
            printf ("Current # of threads in pool = %d\n", m_threadMap.size ());

            // pick a thread who is not busy to kill
            if (this->m_threadMap.size () <= m_threads)
            {
                printf ("Unable to delete threads in pool since min has "
                        "been reached\n");
            }
            else
            {
                if (DESTROY)
                {
                    if (DEBUG)
                        printf ("Ready to destroy some threads\n");
                    destroyThread ();
                }
            }
        }
    }

    StubThread *worker = m_pool.remove ();

    if (!worker)
    {
        printf ("Fetch: Worker not initialized\n");
    }

    *th = worker;

    printf ("Release lock in ThreadPool::fetchWork ()\n");
    m_mutexSyncPool->unlock ();

    m_condAvailThreads->signal ();

    return TRUE;
}

void * ThreadPool::run (uint32 threadID)
{
    StubThread *worker = NULL;

    while (fetchWork (&worker))
    {
        if (DEBUG)
            printf ("In ThreadPool::run()\n");

        if (worker)
        {
            if (DEBUG)
                printf ("Thread #%lu:[%lu]\n", threadID, worker);

            m_mutexWorkIncomplete->lock ();
            m_threadMap[threadID]->setBusy (TRUE);
            m_mutexWorkIncomplete->unlock ();

            worker->executeThis (threadID);

            m_mutexWorkIncomplete->lock ();
            m_threadMap[threadID]->setBusy (FALSE);
            m_mutexWorkIncomplete->unlock ();

            printf ("Thread #%lu finished\n", threadID);
            delete worker;
            worker = NULL;
        }
        else
            perror ("Something wrong in ThreadPool::run()\n");

        m_mutexWorkIncomplete->lock ();
        m_incomplete--;
        if (DEBUG)
            printf ("Incomplete Work after run = %d\n", m_incomplete);
        m_mutexWorkIncomplete->unlock ();
    }
    return 0;
}

ThreadPool::~ThreadPool ()
{
    /* What happens if a thread is not finished? */
    printf ("Trying to delete threadpool!\n");
    while (m_incomplete > 0)
    {
        m_mutexWorkIncomplete->lock ();
        printf ("There is still work %d to be done!\n", m_incomplete);
        m_mutexWorkIncomplete->unlock ();
        sleep (2);
    }

    printf ("Mucho gusto baby!\n");

    m_pool.clear ();

    // Need to explicitly delete each thread in m_threadMap
    for (std::map<uint32, Thread*>::iterator it = m_threadMap.begin ();
         it != m_threadMap.end (); ++it)
    {
        delete it->second;
    }

    m_threadMap.clear ();

    /* Delete Mutexes */
    delete m_condAvailThreads;
    delete m_condAvailWork;
    delete m_mutexSyncPool;
    delete m_mutexWorkIncomplete;

    if (DEBUG)
        printf ("Deleted ThreadPool\n");
}

bool ThreadPool::addThread ()
{
    WorkerThread *th = new WorkerThread (this);
    th->start ();
    th->setBusy (FALSE);
    m_threadMap[(uint32) th->self ()] = th;
    return TRUE;
}

void ThreadPool::destroyThread ()
{
    /*
     * If a thread has been waiting too long for work,
     * attempt to kill it. 
     */
    // Find first thread that is not busy
    std::map<uint32, Thread*>::iterator it;
    it = m_threadMap.begin ();
    if (DEBUG)
        printf ("Current size in destroy thread = %d\n", m_threadMap.size ());
    bool flag = TRUE;
    // Kill only a single idle thread
    //    while (flag)
    //    {
    //        printf ("Thread[%lu]: State[%d]\n",
    //                it->first, it->second->isBusy ());
    //
    //        if (!it->second->isBusy ())
    //        {
    //            printf ("Thread[%lu] is not busy: %d!\n",
    //                    it->first,
    //                    it->second->isBusy ());
    //            delete it->second;
    //            m_threadMap.erase (it);
    //            flag = FALSE;
    //        }
    //        else ++it;
    //    }

    // Kill all idle threads to reduce m_threadMap to size mThreads
    uint32 totalThreads = m_threadMap.size ();
    uint32 i = 0;
    while (flag)
    {
        if (DEBUG)
            printf ("Thread[%lu]: State[%d]\n",
                it->first, it->second->isBusy ());

        if (!it->second->isBusy ())
        {
            if (DEBUG)
                printf ("Thread[%lu] is not busy: %d!\n",
                    it->first,
                    it->second->isBusy ());

            printf ("Removing Thread[%lu] from thread pool!\n",
                    it->first);
            delete it->second;
            m_threadMap.erase (it);
            i += 1;
            ++it;
            if (m_threadMap.size () == m_threads)
                flag = FALSE;
        }
        else ++it;
    }
}

uint32 ThreadPool::getPoolSize ()
{
    return m_threadMap.size ();
}

uint32 ThreadPool::getTimeout ()
{
    return m_keepAlive;
}