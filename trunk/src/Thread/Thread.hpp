#ifndef THREAD_HEADER_H
#define THREAD_HEADER_H

/* Example Thread class taken from blog site 
 * http://vichargrave.com/java-style-thread-class-in-c/
 */

#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include "../Util/Types.hpp"

class Thread
{
private:
    /* Thread ID*/
    pthread_t m_tid;

    /* Flag is 0 when thread is not running and 1 otherwise */
    bool m_running;
    
    /* Flag is 0 when thread is not detached and 1 otherwise*/
    bool m_detached;
        
    /* Flag is 0 when thread is not busy and 1 otherwise */
    bool m_busy;
    
public:
    /* Constructor method */
    Thread ();
    
    /* Destroy a thread */
    virtual ~Thread ();
    
    /* Creates a thread  */
    uint32 start ();
    
    /* Wait for thread to complete */
    uint32 join ();
    
    /* Detach a thread (essentially don't wait for thread to complete) */
    uint32 detach ();
    
    /* Terminate the calling thread */
    void exit ();
    
    /* Return the thread id */
    pthread_t self ();
    
    /* Overload run () method */
    virtual void* run () { return 0; }
    
    /* Assign work */
    void assign (void * fun);
    
    /* Mark thread as done */
    void setBusy (bool res);
    
    /* Check if thread is running */
    bool isBusy ();
    
    /* Check if thread is detached */
    bool isDetached ();
};

#endif /* THREAD_HEADER_H */
