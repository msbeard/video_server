#include "Thread.hpp"

Thread::Thread () : m_tid (0), m_running (false), m_detached (false) { }

/* Check if thread is running and not detached. Call
 * pthread_detach to detach the thread.
 * Check is thread is still running. If so, call
 * thread_cancel to ensure thread is shutdown.
 */
Thread::~Thread ()
{
    //     if (m_running == 1 && m_detached == 0)
    if (m_running && !m_detached)
    {
        if (DEBUG)
            printf ("Detach thread #%lu\n", (uint32) m_tid);
        pthread_detach (m_tid);
    }
    if (m_running)
    {
        if (DEBUG)
            printf ("Cancel thread #%lu\n", (uint32) m_tid);
        pthread_cancel (m_tid);
    }
    if (DEBUG)
        printf ("Deleting thread #%lu\n", (uint32) m_tid);
}

static void* runThread (void* arg)
{
    if (DEBUG)
        printf ("RunThread is being called!\n");
    return ((Thread*) arg)->run ();
}

uint32 Thread::start ()
{
    uint32 result = pthread_create (&m_tid, NULL, runThread, this);
    if (result == 0)
    {
        m_running = true;
    }
    if (DEBUG)
        printf ("Creating thread #%lu\n", (uint32) m_tid);
    return result;
}

uint32 Thread::join ()
{
    uint32 result = -1;
    if (m_running)
    {
        result = pthread_join (m_tid, NULL);
        if (result == 0)
            m_detached = true;
    }
    if (DEBUG)
        printf ("Joining thread #%lu\n", (uint32) m_tid);
    return result;
}

uint32 Thread::detach ()
{
    uint32 result = -1;
    // if (m_running == 1 && m_detached == 0)
    if (m_running && !m_detached)
    {
        result = pthread_detach (m_tid);
        if (result == 0)
            m_detached = TRUE;
    }
    if (DEBUG)
        printf ("Detaching thread #%lu\n", (uint32) m_tid);
    return result;
}

void Thread::exit ()
{
    pthread_exit (NULL);
}

pthread_t Thread::self ()
{
    return m_tid;
}

bool Thread::isBusy ()
{
    return m_busy;
}

bool Thread::isDetached ()
{
    return m_detached;
}

void Thread::setBusy (bool res)
{
//    m_running = res;
    m_busy = res;
}