/*
   client.cpp

   Test client for the tcpsockets classes. 

   ------------------------------------------

   Copyright © 2013 [Vic Hargrave - http://vichargrave.com]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "tcpconnector.h"
#include "../Message/Message.hpp"

using namespace std;
using namespace TCP;

Message createTextMessage (uint32 client, uint32 priority)
{
    std::vector<std::string> argV;
    argV.push_back (std::string (""));
    argV.push_back (std::string (""));
    argV.push_back (std::string (""));
    return Message (client,
            priority,
            std::string ("Hello World!"),
            argV,
            3);
}

int main (int argc, char** argv)
{
    if (argc != 3)
    {
        printf ("usage: %s <port> <ip>\n", argv[0]);
        exit (1);
    }

    int len;
    char const * message;
    char line[256];
    TCPConnector* connector = new TCPConnector ();
    TCPStream* stream = connector->connect (argv[2], atoi (argv[1]));
    if (stream)
    {
//        message = "Is there life on Mars?";
        Message m = createTextMessage (stream->getPeerDesc (), 1);
//        stream->sendto (message.c_str (), message.size ());
        message = m.serialize ();
        
        printf ("Client sending...\n");
        printf ("%d : %d : %s : %d : %s\n", m.getClientID(),
                            m.getPriority (), m.getRequest (), strlen (message),
                message);
        
        stream->send (message, 256);
//        printf ("sent - %s\n", message.c_str ());
//        len = stream->receive (line, sizeof (line));
//        line[len] = NULL;
//        printf ("received - %s\n", line);
        delete stream;
    }

    //    stream = connector->connect(argv[2], atoi(argv[1]));
    //    if (stream) {
    //        message = "Why is there air?";
    //        stream->sendto(message.c_str(), message.size());
    //        printf("sent - %s\n", message.c_str());
    //        len = stream->receive(line, sizeof(line));
    //        line[len] = NULL;
    //        printf("received - %s\n", line);
    //        delete stream;
    //    }
    exit (0);
}
