#include "Mutex.hpp"

Mutex::Mutex () 
{
    if (DEBUG)
        printf("Creating mutex\n");
    pthread_mutex_init (&m_mtx, NULL);
}

Mutex::~Mutex ()
{
    if (DEBUG)
        printf("Deleting mutex\n");
    pthread_mutex_destroy (&m_mtx);
}

uint32 Mutex::lock ()
{
    if (DEBUG)
        printf("Lock a mutex\n");
    return pthread_mutex_lock (&m_mtx);
}

uint32 Mutex::trylock ()
{
    if (DEBUG)
        printf("Attemping to lock a mutex\n");
    return pthread_mutex_trylock (&m_mtx);
}

uint32 Mutex::unlock ()
{
    if (DEBUG)
        printf("Unlocking a mutex\n");
    return pthread_mutex_unlock (&m_mtx);
}