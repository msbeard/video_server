#ifndef MUTEX_HEADER_H
#define MUTEX_HEADER_H

/* Example Mutex class taken from blog site
 * http://vichargrave.com/mutex-class-in-c/
 */

#include <pthread.h>
#include <stdio.h>
#include "../Util/Types.hpp"

class Mutex
{
private:
    pthread_mutex_t m_mtx;
    
    /* Null 
     * pthread_mutexattr_t attr;
     */
    
public:
    /* Create a mutex */
    Mutex ();
    
    /* CondVar class has access to Mutex private variables */
    friend class CondVar;
    
    /* Deletes a mutex */
    virtual ~Mutex ();
    
    /* Locks the mutex blocking on it until the lock is acquired */
    uint32 lock ();
    
    /* Attempt to lock a mutex without blocking */
    uint32 trylock ();
    
    /* Unlocks a mutex */
    uint32 unlock ();
};

#endif /* MUTEX_HEADER_H */