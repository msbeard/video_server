#ifndef QUEUE_HEADER_HPP
#define QUEUE_HEADER_HPP

/* Queue class based on example from this site:
 * http://vichargrave.com/multithreaded-work-queue-in-c/
 */
#include <iostream>
#include <vector>
#include <algorithm>
#include "../Util/Types.hpp"
#include "../Mutex/Mutex.hpp"
#include "../CondVar/CondVar.hpp"

/* Queue class is a thread-safe buffer to add/remove items */
template <typename T> class Queue
{
private:
    /* Internal data structure to store the items */
    std::vector<T> m_queue;

    /* Mutex to lock/unlock the shared Queue */
    Mutex *m_mutex;

    /* Conditional variable */
    CondVar *m_condv;

public:
    /* Queue constructor */
    Queue()
    {
        m_mutex = new Mutex();
        m_condv = new CondVar(*m_mutex);
    }

    void sort ()
    {
        m_mutex->lock ();
        std::sort (m_queue.begin(), m_queue.end(), T());
        m_mutex->unlock();
    }
    
    /* Atomic add an item in the Queue */
    void add(T item)
    {
        m_mutex->lock();
        m_queue.push_back(item);
        m_condv->signal();
        m_mutex->unlock();
    }

    /* Atomic remove an item in the Queue */
    T remove()
    {
        m_mutex->lock();
        while (m_queue.empty())
        {
            printf("Queue: Queue is empty\n");
            m_condv->wait();
        }
        T item = m_queue.front();
        m_queue.erase(m_queue.begin() + 0);
        m_mutex->unlock();
        return item;
    }

    /* Atomic access an element in the Queue */
    T at(uint32 index)
    {
        T item;
        m_mutex->lock();
        item = m_queue.at(index);
        m_mutex->unlock();
        return item;
    }
    
    /* Atomic update in the Queue */
    void update (T item, uint32 index)
    {
        m_mutex->lock();
        m_queue[index] = item;
        m_mutex->unlock();
    }

    /* Atomic check if Queue is empty */
    bool empty ()
    {
        bool result;
        m_mutex->lock();
        result = m_queue.empty();
        m_mutex->unlock();
        return result;
    }

    /* Atomic query size of Queue */
    uint32 size()
    {
        m_mutex->lock();
        uint32 size = m_queue.size();
        m_mutex->unlock();
        return size;
    }

    /* Clear the Queue 
     * Note that this is not safe, especially if there are dynamically
     * creating objects in the queue.
     */
    void clear()
    {
        m_mutex->lock();
        m_queue.clear();
        m_mutex->unlock();
    }

    /* Destructor */
    ~Queue()
    {
        /* Programmer is responsible for deleting elements in the Queue */
        delete m_mutex;
        delete m_condv;
    }
};

#endif /* QUEUE_HEADER_HPP */