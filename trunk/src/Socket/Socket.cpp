#include "Socket.hpp"
#include "../Message/Message.hpp"
#include <sstream>

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
void sigchld_handler(int s)
{
    while(waitpid(-1, NULL, WNOHANG) > 0);
}
//bind and listen to a socket given by portNum
Socket::Socket(int portNum, int backlogSize) {
    //assign members
    std::stringstream ss;
    ss << portNum;
    this->m_portNum = ss.str();
    this->m_backlogSize = backlogSize;
    uint32 sockfd, new_fd;  // listen on sock_fd, new connection on new_fd
    struct sockaddr_storage their_addr; // connector's address information
    socklen_t sin_size;
    struct sigaction sa;
    uint32 yes=1;
    char s[INET6_ADDRSTRLEN];
    struct addrinfo hints, *servinfo, *p;
    uint32 addrInfoStatus; //will be 0 on success.
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE; // use my IP

    if ((addrInfoStatus = getaddrinfo(NULL, this->m_portNum.c_str(), &hints, &servinfo)) != 0) {
        fprintf(stderr, "Error getting address status for portNum: %s\n", gai_strerror(addrInfoStatus));
    }
    // loop through all the results and bind to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                p->ai_protocol)) == -1) {
            perror("server: socket");
            continue;
        }

        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
                sizeof(int)) == -1) {
            perror("setsockopt");
            exit(1);
        }

        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("server: bind");
            continue;
        }

        break;
    }

    if (p == NULL)  {
        fprintf(stderr, "server: failed to bind\n");
    }

    freeaddrinfo(servinfo); // all done with this structure

    if (listen(sockfd, this->m_backlogSize) == -1) {
        perror("listen");
        exit(1);
    }

    sa.sa_handler = sigchld_handler; // reap all dead processes
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        perror("sigaction");
        exit(1);
    }

    printf("server: waiting for connections...\n");
    while(1) {  // main accept() loop
        sin_size = sizeof their_addr;
        new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
        if (new_fd == -1) {
            perror("accept");
            continue;
        }

        inet_ntop(their_addr.ss_family,
            get_in_addr((struct sockaddr *)&their_addr),
            s, sizeof s);
        printf("server: got connection from %s\n", s);

       // if (!fork()) { // this is the child process
            close(sockfd); // child doesn't need the listener
            char buffer[256];
            recv(new_fd, buffer, 256, 0);
            std::cout << "received: " << buffer << std::endl;
            //if (send(new_fd, "Hello, world!", 13, 0) == -1)
                //perror("send");
            close(new_fd);
            std::exit(0);
       // }
        close(new_fd);  // parent doesn't need this
        break;
    }

}


