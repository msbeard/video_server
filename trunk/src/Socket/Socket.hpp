#ifndef SOCKET_HEADER_HPP
#define SOCKET_HEADER_HPP

#include <sys/types.h>       // For data types
#include <sys/socket.h>      // For socket(), connect(), send(), and recv()
#include <sys/wait.h>        
#include <netdb.h>           // For gethostbyname()
#include <arpa/inet.h>       // For inet_addr()
#include <unistd.h>          // For close()
#include <netinet/in.h>      // For sockaddr_in
#include <iostream>
#include <signal.h>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <string.h> //memset

#include "../Util/Types.hpp"

class Socket {

private:
    struct addrinfo m_hints;
    uint32 m_backlogSize;
    std::string m_portNum;
    
public:
    //odd to ask for char* but its what getaddrinfo uses
    Socket(int portNum, int backlogSize);
   
    
};
#endif /* SOCKET_HEADER_HPP */
