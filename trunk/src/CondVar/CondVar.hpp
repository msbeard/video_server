#ifndef CONDVAR_HEADER_H
#define CONDVAR_HEADER_H

/* Example CondVar class taken from 
 * http://vichargrave.com/condition-variable-class-in-c/
 */

#include <pthread.h>
#include <stdio.h>
#include <time.h>
#include "../Mutex/Mutex.hpp"
#include "../Util/Types.hpp"

class CondVar
{
private:
    Mutex& m_mutex;
    pthread_cond_t m_cond;
    
    /* Null 
     * pthread_condattr_t attr;
     */
    CondVar ();
    
public:
    /* Create a conditional variable */
    CondVar (Mutex &);
    
    /* Deletes a conditional variable */
    virtual ~CondVar ();
    
    /* Waits on a conditional variable */
    uint32 wait ();
    
    /* Wait no longer than the specified time for a condition and lock the 
     * specified mutex
     */
    uint32 timedwait (uint32 sec);
    
    /* Unlock a thread waiting for a conditional variable */
    uint32 signal ();
    
    /* Unblock all threads waiting on a conditional variable */
    uint32 broadcast ();
};

#endif /* CONDVAR_HEADER_H */