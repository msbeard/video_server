#include "CondVar.hpp"
#include <time.h>

CondVar::CondVar (Mutex &mtx) : m_mutex (mtx)
{
    if (DEBUG)
        printf("Creating conditional variable\n");
    pthread_cond_init (&m_cond, NULL);
}

CondVar::~CondVar ()
{
    if (DEBUG)
        printf("Deleting conditional variable\n");
    pthread_cond_destroy (&m_cond);
}

uint32 CondVar::wait ()
{
    if (DEBUG)
        printf("Waiting on conditional variable\n"); 
    return pthread_cond_wait (&m_cond, &m_mutex.m_mtx);
}

uint32 CondVar::timedwait (uint32 sec)
{
    if (DEBUG)
        printf("Wait for some period of time %lu\n", sec);
    struct timespec ts;
    ts.tv_sec = time(NULL) + sec;
    ts.tv_nsec = 0;
    return pthread_cond_timedwait (&m_cond, &m_mutex.m_mtx, &ts);
}

uint32 CondVar::signal ()
{
    if (DEBUG)
        printf("Unblock a waiting thread\n");
    return pthread_cond_signal (&m_cond);
}

uint32 CondVar::broadcast ()
{
    if (DEBUG)
        printf("Unblocking all threads\n");
    return pthread_cond_broadcast (&m_cond);
}