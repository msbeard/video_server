To Build all tests:

make

To Build a specific test file:

make MsgServer.exe

To Test:

./MsgServer.exe

To Clean Generated Object Files & Executables:

make clean

-- Michelle
