/*
 * File:   ThreadTest.cpp
 * Author: msbeard
 *
 * Created on Nov 3, 2013, 10:15:19 PM
 *
 * Create two threads that display a message while
 * main thread waits for a change in the global variable's
 * value.
 */

#include <stdlib.h>
#include <iostream>

#include "../../src/VideoServer.h"

uint32 value;

class ThreadTest : public Thread
{
private:
    Mutex &m_mutex;
    CondVar &m_cond;
    char const * m_msg;

public:

    ThreadTest (Mutex &mutex, CondVar &cond, char const * msg) :
    m_mutex (mutex), m_cond (cond), m_msg (msg)
    {
    }

    void *run ()
    {
        for (uint32 i = 0; i < 3; i++)
        {
            printf ("Thread #%lu waiting on mutex.\n", (uint32) self ());
            m_mutex.lock ();
            printf ("Thread #%lu got mutex.\n", (uint32) self ());
            printf ("Thread #%lu says %s\n", (uint32) self (), m_msg);
            sleep (2);
            value = 1;
            m_mutex.unlock ();
            m_cond.signal ();
        }
        printf ("Thread #%lu done!\n", (uint32) self ());
        return NULL;
    }
};

class WorkerThread : public Thread
{
private:
    Queue<Message*> &m_queue;
    //    Message &m_msg;

public:

    WorkerThread (Queue<Message*> &queue) : m_queue (queue)
    {
    }

    void *run ()
    {
        // Take work from queue and print message inside of it
        Message *m;
        m = (Message *) m_queue.remove (); // atomic operation
        printf ("Thread #%lu says %s!\n", (uint32) self (), m->getRequest ());
        return 0;
    }
};

class HelloThread : public StubThread
{
private:
    uint32 m_id;
public:

    HelloThread (uint32 id) : m_id (id)
    {
        if (DEBUG)
            std::cout << "Created HelloThread[" << m_id
                << "] Address:[" << this << "]" << std::endl;
    }

    unsigned virtual executeThis ()
    {
        printf ("Client %d: Hello World! \n", m_id);
        sleep(2);
    }

    ~HelloThread ()
    {
        if (DEBUG)
            std::cout << "Deleting HelloThread " << m_id << "\t address=" << this << std::endl;
    }
};

void test1 ()
{
    Mutex mutex;
    CondVar cond (mutex);
    char const * msg1 = "Hello, World!";
    char const * msg2 = "Sekai e, Konnichiwa!";
    ThreadTest *t1 = new ThreadTest (mutex, cond, msg1);
    ThreadTest *t2 = new ThreadTest (mutex, cond, msg2);

    t1->start ();
    t2->start ();

    // Wait for thread to change value
    printf ("main () waiting on mutex\n");
    mutex.lock ();
    printf ("main () has mutex\n");
    while (value == 0)
    {
        cond.wait ();
    }
    printf ("Detected change in value\n");
    mutex.unlock ();

    t1->join ();
    t2->join ();
    printf ("Main Done!\n");
    delete t1;
    delete t2;
}

void test2 ()
{
    ThreadPool *tp = new ThreadPool (5, 10, 30);
    tp->init ();

    for (uint32 i = 0; i < 100; i++)
    {
        HelloThread *th = new HelloThread (i);
        if (!th)
        {
            printf ("Unable to create hello thread!\n");
            exit (-1);
        }
        tp->assignWork (th);
        if (i % 5 == 0) 
            tp->addThread ();
    }

    printf ("Trying to delete Thread pool in test2\n");
    delete tp;
}

int main (int argc, char** argv)
{
    //    test1 ();
    test2 ();
    return (EXIT_SUCCESS);
}