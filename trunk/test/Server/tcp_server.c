#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <cstdio>
#include <cstdlib>
#include "../../src/Message/Message.hpp"
#include <iostream>

void servConn(int port)
{

    int sd, new_sd;
    struct sockaddr_in name, cli_name;
    int sock_opt_val = 1;
    socklen_t cli_len = sizeof (struct sockaddr_in);
    char data[256]; /* Our receive data buffer. */

    if ((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("(servConn): socket() error");
        exit(-1);
    }

    if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (char *) &sock_opt_val,
                   sizeof (sock_opt_val)) < 0)
    {
        perror("(servConn): Failed to set SO_REUSEADDR on INET socket");
        exit(-1);
    }

    name.sin_family = AF_INET;
    name.sin_port = htons(port);
    name.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(sd, (struct sockaddr *) &name, sizeof (name)) < 0)
    {
        perror("(servConn): bind() error");
        exit(-1);
    }

    listen(sd, 5);
    new_sd = accept (sd, (struct sockaddr *) &cli_name, &cli_len);
    
    while (new_sd)
    {
        //      cli_len = sizeof (cli_name);
        //      new_sd = accept (sd, (struct sockaddr *) &cli_name, &cli_len);
        //      printf ("Assigning new socket descriptor:  %d\n", new_sd);
        //      
        //      if (new_sd < 0) {
        //        perror ("(servConn): accept() error");
        //        exit (-1);
        //      }
        //
        //      if (fork () == 0) {       /* Child process. */
        //        close (sd);
        //        read (new_sd, &data, 14); /* Read our string: "Hello, World!" */
        recv(new_sd, &data, 256, 0);

        // Unmarshall data!
        Message m2;
        m2.deSerialize2(data);
        std::cout << m2.getClientID() << std::endl;
        std::cout << m2.getPriority() << std::endl;
        std::cout << m2.getRequest() << std::endl;
        for (uint32 i = 0; i < m2.getArgumentsCount(); i++)
            std::cout << m2.getArguments()[i] << std::endl;
        std::cout << m2.getArgumentsCount() << std::endl;

        close(new_sd);
        exit (0);
    }
//    close (new_sd);
    close (sd);
}

int main()
{
    servConn(3490);
    return 0;
}

