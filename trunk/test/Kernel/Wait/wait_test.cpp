/*
 * wait_test.cpp
 *
 * Tests the conditional wait function.
 * 
 * Instructions:
 * 1.  insmod my_mutex_module.ko
 * 2.  ./wait_test.exe &
 * 3.  ./signal_test.exe &
 *
 */

#include <stdio.h>
#include <fcntl.h>      // open
#include <sys/ioctl.h>  // ioctl

#include "../../../src/Util/Types.hpp"

// Main
int main()
{
    my_mutex_id id;
    int fd;

    printf("Running conditional wait test\n");

    // Open proc file
    fd = open("/proc/my_mutex", O_RDONLY);

    if (fd < 0)
    {
        printf("Error opening /proc/my_mutex\n");
        return 1;
    }

    // Set mutex ID
    id.mutex = 2;
    id.condv = 0;

    // Lock mutex
    printf("Running my_mutex_lock()\n");
    ioctl(fd, MY_MUTEX_LOCK, &id);

    // Conditional wait
    printf("Running my_cond_wait()\n");
    ioctl(fd, MY_COND_WAIT, &id);

    // Run signal_test.exe to wake up this process

    // Unlock mutex
    printf("Running my_mutex_unlock()\n");
    ioctl(fd, MY_MUTEX_UNLOCK, &id);

    return 0;
}
