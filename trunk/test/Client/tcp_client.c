#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <iostream>
#include "../../src/Message/Message.hpp"

using namespace std;

int cliConn(const char *host, int port)
{

    struct sockaddr_in name;
    struct hostent *hent;
    int sd;

    if ((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("(cliConn): socket() error");
        exit(-1);
    }

    if ((hent = gethostbyname(host)) == NULL)
        fprintf(stderr, "Host %s not found.\n", host);
    else
        bcopy(hent->h_addr, &name.sin_addr, hent->h_length);

    name.sin_family = AF_INET;
    name.sin_port = htons(port);

    /* connect port */
    if (connect(sd, (struct sockaddr *) &name, sizeof (name)) < 0)
    {
        perror("(cliConn): connect() error");
        exit(-1);
    }

    return (sd);
}

int main()
{
    int sd = cliConn("localhost", 3490);
    std::vector<std::string>v1;
    v1.push_back(std::string("arg1"));
    v1.push_back(std::string("arg2"));
    v1.push_back(std::string("arg3"));

    char const * msg = "request";
    Message m1(6, 1, msg, v1, 3);

    char const * test = m1.serialize();
    Message m2;
    m2.deSerialize2 (test);

    std::cout << m2.getClientID() << std::endl;
    std::cout << m2.getPriority() << std::endl;
    std::cout << m2.getRequest() << std::endl;
    for (uint32 i = 0; i < m2.getArgumentsCount(); i++)
        std::cout << m2.getArguments()[i] << std::endl;
    std::cout << m2.getArgumentsCount() << std::endl;
    
    /*printf("Mssage = %\n", test);*/
    send(sd, m1.serialize(), 256, 0);
    close(sd);
    return 0;
}

