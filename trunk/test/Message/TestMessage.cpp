#include "../../src/Message/Message.hpp"
#include "../../src/Util/Types.hpp"
#include "../../src/Queue/Queue.hpp"
#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <algorithm> // std::sort

void testSort ()
{
    uint32 i;
//    std::vector<Message> vec;
    Queue<Message> vec;
    std::vector<std::string> argV;
    argV.push_back (std::string ("arg1"));
    argV.push_back (std::string ("arg2"));
    argV.push_back (std::string ("arg3"));
    
    /* initialize random seed */
    srand (time (NULL));
    
    for (i = 0; i < 10; i++)
    {
        // r in the range 1 to 100
        int r = rand() % 100 + 1;     
        vec.add(Message (i, r, "", argV, 3));
    }
    
    // Before sort
    printf ("Before sorting\n");
    for (i = 0; i < 10; i++)
    {
//       Message m = vec[i];
       Message m = vec.at(i);
       std::cout << "Client #" << m.getClientID () << " : " << m.getPriority () << std::endl;
    }
    
//    std::sort (vec.begin(), vec.end(), Message());
    vec.sort ();
    // After sort
    printf ("After sorting\n");
    for (i = 0; i < 10; i++)
    {
        Message m = vec.at(i);
       std::cout << "Client #" << m.getClientID () << " : " << m.getPriority () << std::endl;
    }
}

int main ()
{
    Message *testMessage;
    std::vector<std::string> argV;
    argV.push_back (std::string ("arg1"));
    argV.push_back (std::string ("arg2"));
    argV.push_back (std::string ("arg3"));

    testMessage = new Message (200,
                               2,
                               std::string ("hi"),
                               argV,
                               3);
    std::cout << "Created message" << std::endl;
    const char* test = testMessage->serialize ();
    //    printf("Serialized message = %s\n", test);
    Message m2;
    m2.deSerialize2(test);

    std::cout << m2.getClientID () << std::endl;
    std::cout << m2.getPriority () << std::endl;
    std::cout << m2.getRequest () << std::endl;
    for (uint32 i = 0; i < m2.getArgumentsCount(); i++)
        std::cout << m2.getArguments ()[i] << std::endl;
    std::cout << m2.getArgumentsCount () << std::endl;

    delete testMessage;
    
    testSort ();
}