/* 
 * File:   Server.cpp
 * Author: msbeard
 *
 * Created on Nov 3, 2013, 10:15:19 PM
 */

#include <stdlib.h>
#include <iostream>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <cstdio>
#include <cstdlib>

#include "../../src/ThreadPool/ThreadPool.hpp"
#include "../../src/Thread/Thread.hpp"
#include "../../src/Mutex/Mutex.hpp"
#include "../../src/CondVar/CondVar.hpp"
#include "../../src/Util/Types.hpp"
#include "../../src/Message/Message.hpp"
#include "../../src/Queue/Queue.hpp"

// With full disclosure,
// using TCPSocket class written by vicargrave
// I found his OO style appealing, and easy to understand.
#include "../../src/TCPSockets/tcpacceptor.h"
#include "../../src/TCPSockets/tcpconnector.h"
#include "../../src/TCPSockets/tcpstream.h"

#define MAXSLOTS 10
#define BUFFERSIZE 256
/*
 * Dispatcher executes when the buffer has at least m slots full. It
 * reorders the buffer by priority and then sends m messages to the 
 * clients. m is a tunable parameter between 1 - MAXSLOTS.
 */
class Dispatcher : public Thread
{
private:
    Queue<Message> &m_workqueue;
    uint32 m_numClients;
    Mutex &m_mutex;
    CondVar &m_condEmpty;
    CondVar &m_condFull;
    uint32 m_tsec;
public:

    Dispatcher (Queue<Message> &queue,
            uint32 tsec,
            Mutex &mutex,
            CondVar &condEmpty,
            CondVar &condFull) :
    m_workqueue (queue),
    m_tsec (tsec),
    m_mutex (mutex),
    m_condEmpty (condEmpty),
    m_condFull (condFull) { };

    void * run ()
    {
        if (DEBUG)
            printf ("Dispatcher is running: %lu\n", (uint32) this->self ());

        uint32 numMsgs = 1;
        while (1)
        {
            m_mutex.lock ();

            // Verify Queue Size not empty
            while (m_workqueue.size () < MAXSLOTS)
                //            while (m_workqueue.empty ())
            {
                printf ("Dispatcher: QueueSize = %d\n",
                        m_workqueue.size ());
                printf ("Blocking until slot count is reached\n");
                m_condEmpty.wait ();
            }
            printf ("Dispatcher: QueueSize = %d\n",
                    m_workqueue.size ());
            // Sort WorkQueue by priority
            m_workqueue.sort ();

            // Enter critical section
            while (!m_workqueue.empty ())
            {
                if (DEBUG)
                    printf ("Dispatcher: QueueSize = %d\n",
                        m_workqueue.size ());

                Message m = m_workqueue.remove ();

                printf ("Dispatcher #%lu: Client # %lu : %d : %d\n",
                        (uint32) this->self (),
                        m.getClientID (),
                        numMsgs,
                        m.getPriority ());
                numMsgs += 1;

            }
            // Exit critical section
            m_mutex.unlock ();
            m_condFull.signal ();
        }
        printf ("Dispatcher #%lu is done\n", (uint32) this->self ());
        printf ("Dispatcher completed %d messages\n", numMsgs - 1);
    }
};

/*
 * Worker threads create either text or video messages to be added to 
 * the buffer. 
 */
class Worker : public StubThread
{
private:
    uint32 m_id; /* Client Descriptor */
    char const * m_msg; /* Message received from server */
    uint32 m_port; /* Port to send message back to */
    Queue<Message> &m_workqueue;
    Mutex &m_mutex;
    CondVar &m_condEmpty;
    CondVar &m_condFull;

public:

    Worker (uint32 id,
            char const *msg,
            uint32 port,
            Queue<Message> &queue,
            Mutex &mutex,
            CondVar &condEmpty,
            CondVar &condFull)
    : StubThread (id),
    m_id (id),
    m_msg (msg),
    m_port (port),
    m_workqueue (queue),
    m_mutex (mutex),
    m_condEmpty (condEmpty),
    m_condFull (condFull) { };

    Message createTextMessage (uint32 i)
    {
        std::vector<std::string> argV;
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));

        // Unmarshall client data
        Message m;
        m.deSerialize2 (m_msg);
        // Create new message 
        std::string newMsg = "";
        char buffer [BUFFERSIZE];
        sprintf (buffer, "%d:%d:%d:%s", m_id, m.getClientID (), i, m.getRequest ());
        printf ("Worker %d : P = %d\n", m_id, m.getPriority ());
        return Message (m.getClientID (), m.getPriority (), buffer, argV, 3);
    }

    void createVideoMessage () { }

    unsigned virtual executeThis ()
    {
        uint32 i = 1;
        while (i <= 10)
        {
            // Wait until slots are available to put message in
            m_mutex.lock ();

            while (m_workqueue.size () == MAXSLOTS)
            {
                printf ("Blocking until slots are open.\n");
                m_condFull.wait ();
            }

            // Enter Critical section 
            Message m = createTextMessage (i);

            m_workqueue.add (m);

            //            if (DEBUG)
            printf ("Thread #%lu: Msg # = %d\n", m_id, i);

            i++;

            printf ("Thread #%lu: QueueSize = %d\n", m_id, m_workqueue.size ());

            m_mutex.unlock ();

            m_condEmpty.signal ();
            // Exit Critical Section
        }

        printf ("Thread #%lu is done\n", m_id);
    }

    ~Worker () { }
};

/*
 * Server listens on specific port for incoming client requests.
 * When a client request is received, the work is forward to Worker threads.
 * The Server manages the thread pool! If there are more clients than
 * pool threads, the Server will add a new thread to the pool. 
 */
class Server : public Thread
{
private:
    uint32 m_port;
    char const * m_addr;
    uint32 m_poolSize;

    ThreadPool *tp;
    Dispatcher *d;

    // Shared Buffer
    Queue<Message> m_queue;

    // Mutex/CondVar
    Mutex &m_mutex;
    CondVar &m_condEmpty;
    CondVar &m_condFull;

public:

    Server (uint32 port, char const * addr, uint32 poolSize, Queue<Message> queue,
            Mutex &mutex, CondVar &condEmpty, CondVar &condFull) :
    m_port (port), m_addr (addr), m_poolSize (poolSize), m_queue (queue),
    m_mutex (mutex), m_condEmpty (condEmpty), m_condFull (condFull)
    {
        tp = new ThreadPool (m_poolSize, 30);
        tp->init ();

        // Create Dispatcher Thread
        d = new Dispatcher (m_queue, 1, m_mutex, m_condEmpty, m_condFull);
    };

    ~Server ()
    {
        printf ("Shutting down server\n");
        delete d;
        delete tp;
    }

    void *run ()
    {
        d->start ();
        TCP::TCPStream *stream = NULL;
        TCP::TCPAcceptor* acceptor = NULL;
        uint32 clientCnt = 0;
        uint32 clientDesc = 0;

        acceptor = new TCP::TCPAcceptor (m_port, m_addr);

        if (acceptor->start () == 0)
        {
            while (1)
            {
                stream = acceptor->accept ();
                if (stream != NULL)
                {
                    size_t len;
                    char data[BUFFERSIZE];
                    while ((len = stream->receive (data, sizeof (data))) > 0)
//                    while (stream->getPeerDesc ())
                    {
                        stream->receive (data, BUFFERSIZE);
                        clientDesc = stream->getPeerDesc ();
                        clientCnt += 1;
//                        data[len] = NULL;
                        printf ("Server received request from client %d!\n", clientDesc);

                        if (1)
                        {
                            // Deserialize message to check if
                            // sent correctly
                            Message m2;
                            m2.deSerialize2 (data);
                            std::cout << m2.getClientID () << std::endl;
                            std::cout << m2.getPriority () << std::endl;
                            std::cout << m2.getRequest () << std::endl;
                        }
                        
                        Worker *th;
                        th = new Worker (
                                clientDesc,
                                data,
                                m_port,
                                m_queue,
                                m_mutex,
                                m_condEmpty,
                                m_condFull);

                        if (!th)
                        {
                            printf ("Server unable to create worker!\n");
                            //                            exit (-1);
                        }

                        // If there are more clients than threads, add more
                        if (clientCnt > m_poolSize)
                            tp->addThread ();

                        // Create worker to process request
                        tp->assignWork (th);
                    }
                }
            }
        }
    }
};

int main (int argc, char** argv)
{
    if (argc < 3 || argc > 4)
    {
        printf ("usage: %s <hostname> <port> \n", argv[0]);
        exit (-1);
    }

    char const * host = argv[1];
    uint32 port = atoi (argv[2]);

    // Shared Buffer
    Queue<Message> queue;

    Mutex m;
    CondVar empty (m);
    CondVar full (m);


    // Create Server
    Server *s = new Server (port, host, 2, queue, m, empty, full);
    s->start ();

    s->join ();
    //    d->join ();

    delete s;
    //    delete d;

    printf ("Server is done!\n");
    return 0;
}