/* 
 * File:   Client.cpp
 * Author: msbeard
 *
 * Created on Nov 3, 2013, 10:15:19 PM
 */

#include <stdlib.h>
#include <iostream>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>

// Video Server Includes
#include "../../src/ThreadPool/ThreadPool.hpp"
#include "../../src/Thread/Thread.hpp"
#include "../../src/Mutex/Mutex.hpp"
#include "../../src/CondVar/CondVar.hpp"
#include "../../src/Util/Types.hpp"
#include "../../src/Message/Message.hpp"
#include "../../src/Queue/Queue.hpp"

// With full disclosure,
// using TCPSocket class written by vicargrave
// I found his OO style appealing, and easy to understand.
#include "../../src/TCPSockets/tcpacceptor.h"
#include "../../src/TCPSockets/tcpconnector.h"
#include "../../src/TCPSockets/tcpstream.h"

#define PR 10  /* Priority range is from 1:PR */
#define ID 200  /* Client ids range from 1:ID */
#define BUFFERSIZE 256

using namespace TCP;

class ClientThread : public Thread
{
private:
    char const * m_host;
    uint32 m_port;
public:

    ClientThread (char const * host, uint32 port) :
    m_host (host), m_port (port)
    {
        printf ("Created Client Thread\n");
        /* initialize random seed */
        srand (time (NULL));
    };

    Message createTextMessage (uint32 id, uint32 priority)
    {
        std::vector<std::string> argV;
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));
        printf ("ID = %d\n", id);
        printf ("Priority = %d\n", priority);
        Message m1 (id, priority, std::string ("Hello World!"),  argV, 3);
        return m1;
    }

    void createVideoMessage (uint32 priority) { }

    void * run ()
    {
        char data[256]; // buffer

        printf ("Attempting to connect to server\n");
        TCP::TCPConnector * connector = new TCP::TCPConnector ();
        TCP::TCPStream* stream = connector->connect (m_host, m_port);
        if (stream)
        {
            // Priority == ClientID
            uint32 priority = rand () % PR;
            printf ("Rand = %d\n", priority);
            Message msg = createTextMessage (stream->getPeerDesc (), priority);

            const char * msgData = msg.serialize ();

            if (DEBUG)
            {
                // Deserialize message to check if
                // sent correctly
                Message m2;
                m2.deSerialize2 (msgData);
                std::cout << m2.getClientID () << std::endl;
                std::cout << m2.getPriority () << std::endl;
                std::cout << m2.getRequest () << std::endl;
            }

            stream->send (msgData, BUFFERSIZE);

            printf ("Client %d:%d sent : %s\n",
                    stream->getPeerDesc (),
                    priority,
                    msg.getRequest ());

            delete stream;
            //            // Now need to wait for response from server
            //            TCP::TCPAcceptor *acceptor = NULL;
            //            acceptor = new TCP::TCPAcceptor (m_port, m_host);
            //
            //            if (acceptor->start () == 0)
            //            {
            //                while (1)
            //                {
            //                    stream = acceptor->accept ();
            //                    if (stream != NULL)
            //                    {
            //                        size_t len;
            //                        char line[256];
            //                        uint32 cnt = 0;
            //
            //                        while ((len = stream->receive (line, strlen (line))) > 0)
            //                        {
            //                            len = stream->receive (line, sizeof (line));
            //                            line[len] = NULL;
            //
            //                            // Received Message
            //                            Message servMsg;
            //                            servMsg.deSerialize2 (line);
            //                            cnt += 1;
            //                            printf ("Client %d received message #%d - %s\n",
            //                                    stream->getPeerDesc (),
            //                                    cnt,
            //                                    servMsg.getRequest ());
            //                        }
            //                        delete stream;
            //                    }
            //                }
            //            }
        }
        else printf ("Unable to connect to stream\n");
    }
};

int main (int argc, char** argv)
{
    if (argc < 4 || argc > 5)
    {
        printf ("usage: %s <clients> <host> <port> \n", argv[0]);
        exit (-1);
    }
    uint32 clients = atoi (argv[1]);
    char const * host = argv[2];
    uint32 port = atoi (argv[3]);

    /* initialize random seed */
    srand (time (NULL));

    ClientThread *th;
    for (uint32 i = 0; i < clients; i++)
    {
        //        uint32 id = rand () % ID + 1;
        th = new ClientThread ("localhost", port);
        if (!th)
        {
            printf ("Unable to create Client thread!\n");
            exit (1);
        }
        th->start ();
    }

    th->join ();
    printf ("Clients are done!\n");
    return 0;
}