/*
 * File:   gen100.cpp
 * Author: mi22759
 *
 * Created on November 15, 2013, 5:05 PM
 */

#include <stdlib.h>
#include <iostream>
#include <errno.h>
#include <algorithm>

#include "../../src/VideoServer.h"

#define MAXSLOTS 11
#define NUMWORKERS 4
#define NUMMSGS 100

/*
 * The Dispatcher  is mainly responsible for removing items in the work queue
 * in FIFO order and displaying the messages.
 */
class Dispatcher : public Thread
{
private:
    Queue<Message> &m_workqueue;
    uint32 m_numClients;
    Mutex &m_mutex;
    CondVar &m_condEmpty;
    CondVar &m_condFull;
    uint32 m_tsec;
public:

    Dispatcher (Queue<Message> &queue,
            uint32 tsec,
            Mutex &mutex,
            CondVar &condEmpty,
            CondVar &condFull) :
    m_workqueue (queue),
    m_tsec (tsec),
    m_mutex (mutex),
    m_condEmpty (condEmpty),
    m_condFull (condFull) { };

    void * run ()
    {
        uint32 numMsgs = 1;
        while (numMsgs <= NUMWORKERS * NUMMSGS)
        {
            m_mutex.lock ();

            // Verify Queue Size not empty
            while (m_workqueue.size () < MAXSLOTS) // deadlock?
            {
                printf ("Blocking until slot count is reached\n");
                m_condEmpty.wait ();
            }

            // Sort WorkQueue by priority
            m_workqueue.sort ();

            // Enter critical section
            while (!m_workqueue.empty ())
            {
                if (DEBUG)
                    printf ("Dispatcher: QueueSize = %d\n",
                        m_workqueue.size ());

                Message m = m_workqueue.remove ();

                printf ("Dispatcher #%lu: Client # %lu : %d : %d\n",
                        (uint32) this->self (),
                        m.getClientID (),
                        numMsgs,
                        m.getPriority ());
                numMsgs += 1;

            }
            // Exit critical section
            m_condFull.signal ();
            m_mutex.unlock ();

        }
        printf ("Dispatcher #%lu is done\n", (uint32) this->self ());
        printf ("Dispatcher completed %d messages\n", numMsgs - 1);
    }
};

/*
 * Worker Thread is mainly responsible for generating messages and putting
 * them in the work queue.
 */
class Worker : public Thread
{
private:
    Queue<Message> &m_workqueue;
    uint32 m_client;
    Mutex &m_mutex;
    CondVar &m_condEmpty;
    CondVar &m_condFull;
public:

    Worker (Queue<Message> &queue,
            Mutex &mutex,
            CondVar &condEmpty,
            CondVar &condFull,
            uint32 clients)
    : m_workqueue (queue),
    m_mutex (mutex),
    m_condEmpty (condEmpty),
    m_condFull (condFull),
    m_client (clients) { };

    Message createTextMessage (uint32 priority)
    {
        std::vector<std::string> argV;
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));
        return Message (m_client,
                priority,
                std::string ("Hello World!"),
                argV,
                3);
    }

    void createVideoMessage (uint32 priority) {
 }

    void * run ()
    {
        uint32 i = 1;
        while (i <= NUMMSGS)
        {
            Message m = createTextMessage (i);

            // Wait until slots are available to put message in
            m_mutex.lock ();

            while (m_workqueue.size () == MAXSLOTS) // deadlock?
            {
                printf ("Blocking until slots are open.\n");
                m_condFull.wait ();
            }
            // Enter Critical section
            m_workqueue.add (m);

            if (DEBUG)
                printf ("Thread #%lu: Msg # = %d\n", (uint32) this->self (), i);

            i++;

            m_condEmpty.signal ();
            m_mutex.unlock ();

            // Exit Critical Section
        }

        m_condFull.signal();    // deadlock fix?

        printf ("Thread #%lu is done\n", (uint32) this->self ());
    }
};

int main (int argc, char** argv)
{
    if (argc < 2 || argc > 3)
    {
        printf ("usage: %s <workers>\n", argv[0]);
        exit (-1);
    }
    int workers = atoi (argv[1]);

    // Shared Buffer
    Queue<Message> queue;

    Mutex m;
    CondVar empty (m);
    CondVar full (m);
    Dispatcher *d = new Dispatcher (queue, 1, m, empty, full);
    d->start ();

    Worker *worker;

    // ThreadPool? 
    for (uint32 i = 0; i < NUMWORKERS; i++)
    {
        worker = new Worker (queue, m, empty, full, i + 1);
        worker->start ();

        if (!worker)
        {
            printf ("Unable to create worker!\n");
            exit (1);
        }
    }

    d->join ();
    worker->join ();

    delete d;
    delete worker;

    printf ("gen100 done!\n");
    return 0;
}