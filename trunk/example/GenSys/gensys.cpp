/*
 * File:   gensys100.cpp
 * Author: John Ying
 *
 * Version of gen100.cpp using system calls to kernel module
 * my_mutex.
 */

#include <stdlib.h>
#include <iostream>
#include <errno.h>
#include <algorithm>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "../../src/VideoServer.h"

#define MAXSLOTS 10
#define NUMWORKERS 3
#define NUMMSGS 100

/*
 * The Dispatcher  is mainly responsible for removing items in the work queue
 * in FIFO order and displaying the messages.
 */
class Dispatcher : public Thread
{
private:
    Queue<Message> &m_workqueue;
    uint32 m_numClients;
    //Mutex &m_mutex;
    //CondVar &m_condEmpty;
    //CondVar &m_condFull;
    uint32 m_mutex;
    my_mutex_id m_condEmpty;
    my_mutex_id m_condFull;
    uint32 m_tsec;
public:

    Dispatcher (Queue<Message> &queue,
            uint32 tsec,
            //Mutex &mutex,
            //CondVar &condEmpty,
            //CondVar &condFull
            uint32 mutex,
            my_mutex_id condEmpty,
            my_mutex_id condFull) :
    m_workqueue (queue),
    m_tsec (tsec),
    m_mutex (mutex),
    m_condEmpty (condEmpty),
    m_condFull (condFull) { };

    void * run ()
    {
        uint32 numMsgs = 1;
        while (numMsgs <= NUMWORKERS * NUMMSGS)
        {
            //m_mutex.lock ();
            ioctl(m_mutex, MY_MUTEX_LOCK, &m_condEmpty);

            // Verify Queue Size not empty
            while (m_workqueue.size () < MAXSLOTS) // deadlock?
            {
                printf ("Blocking until slot count is reached\n");
                //m_condEmpty.wait ();
                ioctl(m_mutex, MY_COND_WAIT, &m_condEmpty);
            }

            // Sort WorkQueue by priority
            m_workqueue.sort ();

            // Enter critical section
            while (!m_workqueue.empty ())
            {
                if (DEBUG)
                    printf ("Dispatcher: QueueSize = %d\n",
                        m_workqueue.size ());

                Message m = m_workqueue.remove ();

                printf ("Dispatcher #%lu: Client # %lu : %d : %d\n",
                        (uint32) this->self (),
                        m.getClientID (),
                        numMsgs,
                        m.getPriority ());
                numMsgs += 1;
            }
            // Exit critical section
            //m_condFull.signal ();
            //m_mutex.unlock ();
            ioctl(m_mutex, MY_COND_SIGNAL, &m_condFull);
            ioctl(m_mutex, MY_MUTEX_UNLOCK, &m_condEmpty);
        }
        
        printf ("Dispatcher #%lu is done\n", (uint32) this->self ());
        printf ("Dispatcher completed %d messages\n", numMsgs - 1);
    }
};

/*
 * Worker Thread is mainly responsible for generating messages and putting
 * them in the work queue.
 */
class Worker : public Thread
{
private:
    Queue<Message> &m_workqueue;
    uint32 m_client;
    //Mutex &m_mutex;
    //CondVar &m_condEmpty;
    //CondVar &m_condFull;
    uint32 m_mutex;
    my_mutex_id m_condEmpty;
    my_mutex_id m_condFull;
public:

    Worker (Queue<Message> &queue,
            //Mutex &mutex,
            //CondVar &condEmpty,
            //CondVar &condFull,
            uint32 mutex,
            my_mutex_id condEmpty,
            my_mutex_id condFull,
            uint32 clients)
    : m_workqueue (queue),
    m_mutex (mutex),
    m_condEmpty (condEmpty),
    m_condFull (condFull),
    m_client (clients) { };

    Message createTextMessage (uint32 priority)
    {
        std::vector<std::string> argV;
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));
        return Message (m_client,
                priority,
                std::string ("Hello World!"),
                argV,
                3);
    }

    void createVideoMessage (uint32 priority) { }

    void * run ()
    {
        uint32 i = 1;
        while (i <= NUMMSGS)
        {
            Message m = createTextMessage (i);

            // Wait until slots are available to put message in
            //m_mutex.lock ();
            ioctl(m_mutex, MY_MUTEX_LOCK, &m_condEmpty);

            while (m_workqueue.size () == MAXSLOTS) // deadlock?
            {
                printf ("Blocking until slots are open.\n");
                //m_condFull.wait ();
                ioctl(m_mutex, MY_COND_WAIT, &m_condFull);
            }
            // Enter Critical section
            m_workqueue.add (m);

            if (DEBUG)
                printf ("Thread #%lu: Msg # = %d\n", (uint32) this->self (), i);

            i++;

            //m_condEmpty.signal ();
            //m_mutex.unlock ();
            ioctl(m_mutex, MY_COND_SIGNAL, &m_condEmpty);
            ioctl(m_mutex, MY_MUTEX_UNLOCK, &m_condEmpty);

            // Exit Critical Section
        }

        // After thread exits, need to signal to dispatcher that
        // there is the last bit of work it needs to handle
        //m_condFull.signal ();
        ioctl(m_mutex, MY_MUTEX_LOCK, &m_condFull);
        ioctl(m_mutex, MY_COND_SIGNAL, &m_condFull);
        ioctl(m_mutex, MY_MUTEX_UNLOCK, &m_condFull);
        printf ("Thread #%lu is done\n", (uint32) this->self ());
    }
};

int main (int argc, char** argv)
{
    if (argc < 2 || argc > 3)
    {
        printf ("usage: %s <workers>\n", argv[0]);
        exit (-1);
    }
    int workers = atoi (argv[1]);

    // Shared Buffer
    Queue<Message> queue;

    //Mutex m;
    //CondVar empty (m);
    //CondVar full (m);

    // Mutex IDs
    my_mutex_id empty;
    empty.mutex = 0;
    empty.condv = 0;
    my_mutex_id full;
    full.mutex = 0;
    full.condv = 1;

    // Access mutex system calls
    uint32 m = open("/proc/my_mutex", O_RDONLY);

    if (m < 0)
    {
        printf("Error opening /proc/my_mutex\n");
        return 1;
    }

    Dispatcher *d = new Dispatcher (queue, 1, m, empty, full);
    d->start ();

    Worker *worker;

    // ThreadPool? 
    for (uint32 i = 0; i < NUMWORKERS; i++)
    {
        worker = new Worker (queue, m, empty, full, i + 1);
        worker->start ();

        if (!worker)
        {
            printf ("Unable to create worker!\n");
            exit (1);
        }
    }

    d->join ();
    worker->join ();

    delete d;
    delete worker;

    printf ("gensys100 done!\n");
    return 0;
}