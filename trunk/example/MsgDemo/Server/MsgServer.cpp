/* 
 * File:   Server.cpp
 * Author: msbeard
 *
 * Created on Nov 3, 2013, 10:15:19 PM
 */

#include <stdlib.h>
#include <iostream>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <cstdio>
#include <cstdlib>
#include <pthread.h>
#include <string.h>
#include "../../../src/VideoServer.h"

#define MAXSLOTS 10
#define BUFFERSIZE 256
#define MAXMSGS 100
#define TIMEOUT 20

uint32 CLIENTCNT = 0;
Mutex MUTEXCNT;

/*
 * Dispatcher executes when the buffer has at least m slots full. It
 * reorders the buffer by priority and then sends m messages to the 
 * clients. m is a tunable parameter between 1 - MAXSLOTS.
 */
class Dispatcher : public Thread
{
private:
    char const * m_addr;
    uint32 m_tsec;
    Queue<Message> &m_workqueue;
    Mutex &m_mutex;
    CondVar &m_condEmpty;
    CondVar &m_condFull;
public:

    Dispatcher (
            char const * addr,
            uint32 tsec,
            Queue<Message> &queue,
            Mutex &mutex,
            CondVar &condEmpty,
            CondVar &condFull) :
    m_addr (addr),
    m_tsec (tsec),
    m_workqueue (queue),
    m_mutex (mutex),
    m_condEmpty (condEmpty),
    m_condFull (condFull) { };

    ~Dispatcher () { }

    void * run ()
    {
        if (DEBUG)
            printf ("Dispatcher is running: %lu\n", (uint32) this->self ());

        uint32 numMsgs = 0;
        uint32 res;
        //        while (res != ETIMEDOUT)
        while (1)
        {
            m_mutex.lock ();

            // Verify Queue Size not empty
            while (m_workqueue.size () < MAXSLOTS)
            {
                if (DEBUG)
                    printf ("Blocking until slot count is reached\n");
                m_condEmpty.wait ();
                //                res = m_condEmpty.timedwait (TIMEOUT);
                //                if (res == ETIMEDOUT)
                //                {
                //                    printf ("No work is coming\n");
                //                    break;
                //                }
            }
            // Sort WorkQueue by priority
            m_workqueue.sort ();

            // Enter critical section
            while (!m_workqueue.empty ())
            {
                if (DEBUG)
                    printf ("Dispatcher: QueueSize = %d\n",
                        m_workqueue.size ());

                Message msg = m_workqueue.remove ();
                numMsgs += 1;
                if (DEBUG)
                    printf ("Dispatcher #%lu: Attempting to connect back to client!\n",
                        (uint32) this->self ());

                TCP::TCPConnector * connector = new TCP::TCPConnector ();
                TCP::TCPStream* stream = connector->connect (m_addr, msg.getClientID ());
                if (stream)
                {
                    const char * msgData = msg.serialize ();

                    stream->send (msgData, BUFFERSIZE);

                    printf ("Dispatcher #%lu: Client # %lu : %d : %d\n",
                            (uint32) this->self (),
                            msg.getClientID (),
                            numMsgs,
                            msg.getPriority ());

                    delete stream;
                }
                delete connector;
            }
            // Exit critical section
            m_mutex.unlock ();
            m_condFull.signal ();
        }
        printf ("Dispatcher #%lu is done\n", (uint32) this->self ());
        printf ("Dispatcher completed %d messages\n", numMsgs);
        //        this->setBusy (FALSE);
    }
};

/*
 * Worker threads create either text or video messages to be added to 
 * the buffer. 
 */

class Worker : public StubThread
{
private:
    uint32 m_id; // Thread ID 
    char m_msg[BUFFERSIZE]; // Message received from server 
    uint32 m_port; // Port to send message back to 
    Queue<Message> &m_workqueue; // Shared circular buffer 
    Mutex &m_mutex;
    CondVar &m_condEmpty;
    CondVar &m_condFull;

public:

    Worker (char const *msg,
            uint32 port,
            Queue<Message> &queue,
            Mutex &mutex,
            CondVar &condEmpty,
            CondVar &condFull)
    : StubThread (),
    m_id (0),
    m_port (port),
    m_workqueue (queue),
    m_mutex (mutex),
    m_condEmpty (condEmpty),
    m_condFull (condFull)
    {
        memcpy (m_msg, msg, BUFFERSIZE);
    };

    ~Worker () { }

    Message createTextMessage (uint32 i)
    {
        std::vector<std::string> argV;
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));

        // Unmarshall client data
        Message m;
        m.deSerialize2 (m_msg);
        // Create new message 
        std::string newMsg = "";
        char buffer [BUFFERSIZE];

        sprintf (buffer, "%lu:%d:%d:%s", m_id, m.getClientID (), i, m.getRequest ());
        //        printf ("Worker %d : P = %d\n", m_id, m.getPriority ());
        return Message (m.getClientID (), m.getPriority (), buffer, argV, 3);
    }

    unsigned virtual executeThis (uint32 id)
    {
        m_id = id;
        uint32 i = 1;
        while (i <= MAXMSGS)
        {
            // Wait until slots are available to put message in
            m_mutex.lock ();

            while (m_workqueue.size () == MAXSLOTS)
            {
                if (DEBUG)
                    printf ("Blocking until slots are open.\n");
                m_condFull.wait ();
            }

            // Enter Critical section 
            Message m = createTextMessage (i);

            m_workqueue.add (m);

            if (DEBUG)
                printf ("Thread #%lu: Client #: %d : Msg # = %d\n", m_id, m.getClientID (), i);

            i++;

            if (DEBUG)
                printf ("Thread #%lu: QueueSize = %d\n", m_id, m_workqueue.size ());

            m_mutex.unlock ();

            m_condEmpty.signal ();
            // Exit Critical Section
        }
        m_condFull.signal ();
        printf ("Thread #%lu is done\n", m_id);
        MUTEXCNT.lock ();
        CLIENTCNT--;
        printf ("Worker #%lu: Decrementing counter = %d\n", m_id, CLIENTCNT);
        MUTEXCNT.unlock ();
    }
};

/*
 * Server listens on specific port for incoming client requests.
 * When a client request is received, the work is forward to Worker threads.
 * The Server manages the thread pool! If there are more clients than
 * pool threads, the Server will add a new thread to the pool. 
 */
class Server : public Thread
{
private:
    uint32 m_port;
    char const * m_addr;
    uint32 m_poolSize;

    ThreadPool *tp;
    Dispatcher *d;

    // Shared Buffer
    Queue<Message> m_queue;

    // Mutex/CondVar
    Mutex &m_mutex;
    CondVar &m_condEmpty;
    CondVar &m_condFull;

    TCP::TCPStream *stream;
    TCP::TCPAcceptor* acceptor;
    Worker *th;

public:

    Server (uint32 port, char const * addr, uint32 poolSize, Queue<Message> queue,
            Mutex &mutex, CondVar &condEmpty, CondVar &condFull) :
    m_port (port), m_addr (addr), m_poolSize (poolSize), m_queue (queue),
    m_mutex (mutex), m_condEmpty (condEmpty), m_condFull (condFull)
    {
        tp = new ThreadPool (m_poolSize, 10, 15);
        tp->init ();

        // Create Dispatcher Thread
        uint32 timeOut = 1;
        d = new Dispatcher (m_addr, timeOut, m_queue, m_mutex, m_condEmpty, m_condFull);
        //        d->join ();
        stream = NULL;
        acceptor = NULL;
        th = NULL;
    };

    ~Server ()
    {
        if (DEBUG)
            printf ("Shutting down server\n");

        printf ("Trying to delete dispatcher\n");
        while (d->isBusy ())
        {
            printf ("Dispatcher is busy: %d\n", d->isBusy ());
            sleep (2);
        }
        // Close stream
        printf ("Shutting down stream\n");
        //        delete stream;
        //        delete acceptor;
        delete d;
        printf ("Deleted dispatcher\n");
        delete th;
        printf ("Deleted worker thread\n");
        delete tp;
        printf ("Attempting to flush queue?\n");
    }

    bool checkKillMsg (char const * msg)
    {
        std::string res (msg);
        std::string killMsg = "Kill";

        if (res == killMsg)
            return true;
        return false;
    }

    void *run ()
    {
        uint32 SHUTDOWN = 1;
        d->start ();
        //        uint32 clientCnt = 0;
        Worker *th;

        acceptor = new TCP::TCPAcceptor (m_port, m_addr);

        if (acceptor->start () == 0)
        {
            while (1)
            {
                stream = acceptor->accept ();
                if (stream != NULL)
                {
                    size_t len;
                    char data[BUFFERSIZE];
                    while ((len = stream->receive (data, sizeof (data))) > 0)
                    {
                        stream->receive (data, BUFFERSIZE);

                        //                        clientCnt += 1;
                        MUTEXCNT.lock ();
                        CLIENTCNT++;
                        printf ("Server Incrementing counter = %d\n", CLIENTCNT);
                        MUTEXCNT.unlock ();

                        // Deserialize message to check if
                        // sent correctly
                        Message m2;
                        m2.deSerialize2 (data);
                        printf ("Server received message from: \n");
                        std::cout << m2.getClientID () << std::endl;
                        std::cout << m2.getPriority () << std::endl;
                        std::cout << m2.getRequest () << std::endl;

//                        if (checkKillMsg (m2.getRequest ()))
//                        {
//                            shutdown ();
//                            SHUTDOWN = 0;
//                        }
                        th = new Worker (
                                data,
                                m_port,
                                m_queue,
                                m_mutex,
                                m_condEmpty,
                                m_condFull);

                        if (!th)
                            perror ("Server unable to create worker!\n");

                        // If there are more clients than threads, add more
                        if (CLIENTCNT >= tp->getPoolSize ())
                        {
                            printf ("Server adding a new worker! %d\n", CLIENTCNT);
                            tp->addThread ();
                            if (DEBUG)
                                printf ("Current ThreadPool Size = %d\n",
                                    tp->getPoolSize ());
                        }

                        // Create worker to process request
                        if (DEBUG)
                            printf ("Server assigning work on behalf of client %d\n",
                                m2.getClientID ());
                        tp->assignWork (th);
                    }
                    delete stream;
                }
            }
            delete acceptor;
        }
    }
};

int main (int argc, char** argv)
{
    if (argc < 3 || argc > 4)
    {
        printf ("usage: %s <hostname> <port> \n", argv[0]);
        exit (-1);
    }

    char const * host = argv[1];
    uint32 port = atoi (argv[2]);

    // Shared Buffer
    Queue<Message> queue;

    Mutex m;
    CondVar empty (m);
    CondVar full (m);

    // Create Server
    Server *s = new Server (port, host, 2, queue, m, empty, full);
    s->start ();

    s->join ();

    printf ("Trying to delete server\n");
    delete s;
    printf ("Deleted server\n");

    printf ("Server is done!\n");
    return 0;
}