/* 
 * File:   Client.cpp
 * Author: msbeard
 *
 * Created on Nov 3, 2013, 10:15:19 PM
 */

#include <stdlib.h>
#include <iostream>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>

#include "../../../src/VideoServer.h"

#define PR 10  /* Priority range is from 1:PR */
#define ID 200  /* Client ids range from 1:ID */
#define CPORTST 4000 /* Client ports range from 4000:5000 */
#define BUFFERSIZE 256
#define NUMMSGS 10

using namespace TCP;

class KillThread : public Thread
{
private:
    char const * m_host;
    uint32 m_port;
public:

    KillThread (char const * host, uint32 port) :
    m_host (host), m_port (port)
    {
        printf ("Created Kill Thread\n");
        /* initialize random seed */
        srand (time (NULL));
    };

    Message createTextMessage (uint32 id, uint32 priority)
    {
        std::vector<std::string> argV;
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));
        Message m1 (id, priority, std::string ("Kill"), argV, 3);
        return m1;
    }

    void * run ()
    {
        char data[256]; // buffer

        printf ("Kill #%lu attempting to connect to server\n",
                (uint32) this->self ());
        TCP::TCPConnector * connector = new TCP::TCPConnector ();
        TCP::TCPStream* stream = connector->connect (m_host, m_port);
        if (stream)
        {
            // Priority == ClientID
            uint32 priority = 1;
            // Generate Port Number Client will listen on
            uint32 clientID = rand () % 1000 + CPORTST;

            Message msg = createTextMessage (clientID, priority);

            const char * msgData = msg.serialize ();

            if (DEBUG)
            {
                // Deserialize message to check if sent correctly
                Message m2;
                m2.deSerialize2 (msgData);
                std::cout << m2.getClientID () << std::endl;
                std::cout << m2.getPriority () << std::endl;
                std::cout << m2.getRequest () << std::endl;
            }

            stream->send (msgData, BUFFERSIZE);

            printf ("Kill #%lu sent: %d : %d: %s\n",
                    (uint32) this->self (),
                    clientID,
                    priority,
                    msg.getRequest ());

            delete stream;

            // Now need to wait for response from server
            TCP::TCPAcceptor *acceptor = NULL;
            acceptor = new TCP::TCPAcceptor (clientID, m_host);
        }
        else perror ("Unable to connect to stream\n");
        printf ("Kill #%lu is done\n", (uint32) this->self ());
    }
};

class ClientThread : public Thread
{
private:
    char const * m_host;
    uint32 m_port;
    uint32 m_clientID;
public:

    ClientThread (char const * host, uint32 port, uint32 clientID) :
    m_host (host), m_port (port), m_clientID (clientID)
    {
        printf ("Created Client Thread\n");
        /* initialize random seed */
        srand (time (NULL));
    };

    Message createTextMessage (uint32 id, uint32 priority)
    {
        std::vector<std::string> argV;
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));
        argV.push_back (std::string (""));
        Message m1 (id, priority, std::string ("Hello World!"), argV, 3);
        return m1;
    }

    void * run ()
    {
        char data[256]; // buffer

        printf ("Client #%lu attempting to connect to server\n",
                (uint32) this->self ());
        TCP::TCPConnector * connector = new TCP::TCPConnector ();
        TCP::TCPStream* stream = connector->connect (m_host, m_port);
        if (stream)
        {
            // Priority == ClientID
            uint32 priority = rand () % PR;
            // Generate Port Number Client will listen on
//            uint32 clientID = rand () % 1000 + CPORTST;

            Message msg = createTextMessage (m_clientID, priority);

            const char * msgData = msg.serialize ();

            if (DEBUG)
            {
                // Deserialize message to check if sent correctly
                Message m2;
                m2.deSerialize2 (msgData);
                std::cout << m2.getClientID () << std::endl;
                std::cout << m2.getPriority () << std::endl;
                std::cout << m2.getRequest () << std::endl;
            }

            stream->send (msgData, BUFFERSIZE);

            printf ("Client #%lu sent: %d : %d: %s\n",
                    (uint32) this->self (),
                    m_clientID,
                    priority,
                    msg.getRequest ());

            delete stream;

            // Now need to wait for response from server
            TCP::TCPAcceptor *acceptor = NULL;
            acceptor = new TCP::TCPAcceptor (m_clientID, m_host);
            uint32 cnt = 0;

            if (acceptor->start () == 0)
            {
                while (1)
                {
                    stream = acceptor->accept ();
                    if (stream != NULL)
                    {
                        size_t len;
                        char line[BUFFERSIZE];

                        while ((len = stream->receive (line, sizeof (line))) > 0)
                        {
                            len = stream->receive (line, BUFFERSIZE);

                            // Received Message
                            Message servMsg;
                            servMsg.deSerialize2 (line);
                            cnt += 1;
                            printf ("Client %d received message #%d - %s\n",
                                    m_clientID,
                                    cnt,
                                    servMsg.getRequest ());

                        }
                        delete stream;
                    }
                    if (cnt == NUMMSGS)
                    {
                        printf ("Client %d: All messages have been received\n",
                                m_clientID);
                        break;
                    }
                }
                delete acceptor;
            }
        }
        else 
        {
            perror ("Unable to connect to stream\n");
            delete stream;
//            delete connector;
        }
        printf ("Client #%lu is done\n", (uint32) this->self ());
        delete connector;
    }
};

int main (int argc, char** argv)
{
    if (argc < 4 || argc > 5)
    {
        printf ("usage: %s <clients> <host> <port> \n", argv[0]);
        exit (-1);
    }
    uint32 clients = atoi (argv[1]);
    char const * host = argv[2];
    uint32 port = atoi (argv[3]);

    /* initialize random seed */
    srand (time (NULL));

    ClientThread *th;
    uint32 clientID = 5010;
    for (uint32 i = 0; i < clients; i++)
    {
        th = new ClientThread ("localhost", port, clientID);
        if (!th)
        {
            printf ("Unable to create Client thread!\n");
            exit (-1);
        }
        th->start ();
        clientID++;
    }

    th->join ();
    
    // Create Kill Thread
    // This sends a message to the server to attempt to
    // stop all processing.
    // Understandable, if their is work to be done, the server
    // will allow the threads to complete before shutting down.
    
//    sleep (10);
//    KillThread *kh;
//    kh = new KillThread ("localhost", port);
//    if (!th)
//    {
//        printf ("Unable to create Kill thread!\n");
//        exit (-1);
//    }
//    printf ("Sending kill thread\n");
//    kh->start ();
//    kh->join ();
    
    printf ("Clients are done!\n");
    return 0;
}