/*
 * File:        my_mutex_module.c
 * Started by:  John Ying
 *
 * Kernel module that implements mutexes.
 * 
 * References:
 * - http://www.makelinux.net/ldd3/chp-5-sect-3
 * - Operating Systems Concepts 9th Edition, Chapter 5
 *
 * Installing and removing module:
 * 1.  make
 * 2.  insmod my_mutex_module.ko
 * 3.  open("/proc/my_mutex", O_RDONLY); (in user-level code)
 * 4.  rmmod my_mutex_module
 *
 * Using the module:
 * 1.  Use struct my_mutex_id and specify IDs for mutex and conditional variable.
 *     Values of struct members mutex and conv must be within range of [0, NUM_MUTEX-1]
 *     and [0, NUM_CONDV-1] respectively.
 *       e.g., struct my_mutex_id id;
 *             id.mutex = 2;
 *             id.condv = 3;
 * 2.  Open "/proc/my_mutex" file using open sys call
 * 3.  Run ioctl using codes below:
 *       ioctl(fd, MY_MUTEX_LOCK, &mutex_id)
 *       ioctl(fd, MY_MUTEX_UNLOCK, &mutex_id)
 *       ioctl(fd, MY_COND_WAIT, &mutex_id)
 *       ioctl(fd, MY_COND_SIGNAL, &mutex_id)
 *     Note: You will have to include the preprocessor defines below.
 *
 */

#include <asm/uaccess.h>
#include <linux/errno.h>        // error code
#include <linux/kernel.h>       // printk
#include <linux/module.h>
#include <linux/proc_fs.h>      // proc_dir_entry
#include <linux/sched.h>
#include <linux/semaphore.h>    // semaphore
#include <linux/string.h>
#include <linux/tty.h>          // tty_struct

MODULE_LICENSE("GPL");

typedef unsigned long int uint32;

// IOCTL codes for mutex calls
#define MY_MUTEX_LOCK   _IOW(0, 0, struct my_mutex_id)
#define MY_MUTEX_UNLOCK _IOW(0, 1, struct my_mutex_id)
#define MY_COND_WAIT    _IOW(0, 2, struct my_mutex_id)
#define MY_COND_SIGNAL  _IOW(0, 3, struct my_mutex_id)

#define NUM_MUTEX   4
#define NUM_CONDV   4

// Mutex ID data structure
struct my_mutex_id {
    uint32 mutex;
    uint32 condv;
};

// Mutex data structure
struct my_mutex
{
    // Basic mutex (binary) semaphore
    struct semaphore mtx_sem;

    // Waiting processes semaphores
    struct semaphore wait_sem[NUM_CONDV];

    // Counts of waiting processes
    int wait_count[NUM_CONDV];

    // Signaling processes semaphores
    struct semaphore sig_sem[NUM_CONDV];

    // Counts of signaling processes
    int sig_count[NUM_CONDV];
};

// Mutex array
struct my_mutex mtx[NUM_MUTEX];

// File operations
static struct file_operations my_mutex_fops;

// Proc entry
static struct proc_dir_entry *proc_entry;

// IOCTL
static int my_mutex_ioctl(struct inode *inode,
                          struct file *file,
                          unsigned int cmd,
                          unsigned long arg)
{
    struct my_mutex_id id;
    int ret;

    // Get mutex ID data from user space
    ret = copy_from_user(&id, (struct my_mutex_id *) arg,
        sizeof(struct my_mutex_id));

    // Failed to copy from user space
    if (ret)
    {
        printk("<1>copy_from_user: %d bytes could not be copied\n",
            ret);
        return 1;
    }

    // Mutex index outside of range
    if (id.mutex < 0 || id.mutex >= NUM_MUTEX)
    {
        printk("<1>my_mutex: Mutex index %lu not withing range [%d, %d]\n",
            id.mutex, 0, NUM_MUTEX-1);
        return 1;
    }

    // Conditional variable index outside of range
    if (id.condv < 0 || id.condv >= NUM_CONDV)
    {
        printk("<1>my_mutex: Conditional variable index %lu not withing range [%d, %d]\n",
            id.condv, 0, NUM_CONDV-1);
        return 1;
    }

    // Mutex operations
    switch (cmd)
    {
        // Lock mutex
        case MY_MUTEX_LOCK:
            printk("<1>my_mutex: Locking mutex %lu (count = %d)\n",
                id.mutex, mtx[id.mutex].mtx_sem.count);

            // Lock mutex
            down_interruptible(&mtx[id.mutex].mtx_sem);

            printk("<1>my_mutex: Locked mutex %lu! (count = %d)\n",
                id.mutex, mtx[id.mutex].mtx_sem.count);
            break;

        // Unlock mutex
        case MY_MUTEX_UNLOCK:
            printk("<1>my_mutex: Unlocking mutex %lu (count = %d)\n",
                id.mutex, mtx[id.mutex].mtx_sem.count);

            // Mutex must be locked prior
            if (mtx[id.mutex].mtx_sem.count == 0)
            {
                // Resume a suspended signaling process, otherwise unlock mutex
                if (mtx[id.mutex].sig_count[id.condv] > 0)
                    up(&mtx[id.mutex].sig_sem[id.condv]);
                else
                    up(&mtx[id.mutex].mtx_sem);
            }

            printk("<1>my_mutex: Unlocked mutex %lu! (count = %d)\n",
                id.mutex, mtx[id.mutex].mtx_sem.count);
            break;

        // Conditional wait
        case MY_COND_WAIT:
            printk("<1>my_mutex: Conditional wait %lu for mutex %lu... (count = %d)\n",
                id.condv, id.mutex, mtx[id.mutex].mtx_sem.count);

            // Mutex must be locked prior to conditional wait
            if (mtx[id.mutex].mtx_sem.count == 0)
            {
                // Increment count of waiting processes
                mtx[id.mutex].wait_count[id.condv]++;

                // Resume a suspended signaling process, otherwise unlock mutex
                if (mtx[id.mutex].sig_count[id.condv] > 0)
                    up(&mtx[id.mutex].sig_sem[id.condv]);
                else
                    up(&mtx[id.mutex].mtx_sem);

                // Suspend waiting process
                down_interruptible(&mtx[id.mutex].wait_sem[id.condv]);

                // Signaling process wakes up waiting process

                // Decrement count of waiting processes
                mtx[id.mutex].wait_count[id.condv]--;
            }
            else
            {
                // error
            }

            printk("<1>my_mutex: Conditional wait %lu for mutex %lu complete!\n",
                id.condv, id.mutex);
            break;

        // Conditional signal
        case MY_COND_SIGNAL:
            printk("<1>my_mutex: Conditional signal %lu for mutex %lu... (count = %d)\n",
                id.condv, id.mutex, mtx[id.mutex].mtx_sem.count);

            // Mutex must be locked prior to conditional signal
            if (mtx[id.mutex].mtx_sem.count == 0)
            {
                // Waiting processes in queue
                if (mtx[id.mutex].wait_count[id.condv] > 0)
                {
                    // Increment count of signaling processes
                    mtx[id.mutex].sig_count[id.condv]++;

                    // Resume a suspended waiting process
                    up(&mtx[id.mutex].wait_sem[id.condv]);

                    // Suspend signaling process to wait for waiting process to complete
                    down_interruptible(&mtx[id.mutex].sig_sem[id.condv]);

                    // Waiting process wakes up signaling process

                    // Decrement count of signaling processes
                    mtx[id.mutex].sig_count[id.condv]--;
                }
            }
            else
            {
                // error
            }

            printk("<1>my_mutex: Conditional signal %lu for mutex %lu complete!\n",
                id.condv, id.mutex);
            break;

        // Default
        default:
            return -EINVAL;
            break;
    }

    return 0;
}

// Initilization
static int __init my_mutex_init(void)
{
    int i, j;

    printk("<1>my_mutex: Loading module\n");

    // File operations
    my_mutex_fops.ioctl = my_mutex_ioctl;

    // Proc entry
    proc_entry = create_proc_entry("my_mutex", 0444, NULL);

    if (!proc_entry)
    {
        printk("<1>my_mutex: Error creating proc entry\n");
        return 1;
    }

    proc_entry->proc_fops = &my_mutex_fops;

    // Mutexes
    for (i = 0; i < NUM_MUTEX; i++)
    {
        // Initialize mutex to unlocked
        init_MUTEX(&mtx[i].mtx_sem);

        // Conditional variables
        for (j = 0; j < NUM_CONDV; j++)
        {
            // Initialize conditional variable to locked
            init_MUTEX_LOCKED(&mtx[i].wait_sem[j]);
            init_MUTEX_LOCKED(&mtx[i].sig_sem[j]);

            // Clear process counters
            mtx[i].wait_count[j] = 0;
            mtx[i].sig_count[j] = 0;
        }
    }

    return 0;
}

// Cleanup
static void __exit my_mutex_exit(void)
{
    printk("<1>my_mutex: Dumping module\n");

    // Proc entry
    remove_proc_entry("my_mutex", NULL);
}

// Set init and exit function
module_init(my_mutex_init);
module_exit(my_mutex_exit);
